﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// EasyCode 的摘要描述
/// </summary>
public static class EasyCode
{
    #region "模組名稱"
    /// <summary>
    /// 模組名稱
    /// </summary>
    /// <param name="module">模組名稱</param>
    /// <returns></returns>
    static public string ToEasyModuleName(string module)
    {
        switch (module)
        {
            case "A01":
                return "帳號權限系統";
            case "B01":
                return "網站管理設定";
            case "C01":
                return "首頁管理";
            case "D01":
                return "經營理念";
            case "D02":
                return "我們的優勢";
            case "D03":
                return "組織概要";
            case "D04":
                return "里程碑";
            case "D05":
                return "客戶範圍";
            case "E01":
                return "最新消息";
            case "F01":
                return "品質認證";
            case "F02":
                return "實驗室";
            case "G01":
                return "產品介紹";
            case "G02":
                return "產品維護";
            case "H01":
                return "訓練與發展";
            case "H02":
                return "人才招募";
            case "H03":
                return "福利制度";
            case "H04":
                return "活動花絮";
            case "I01":
                return "下載資訊";
            case "I02":
                return "資源下載";
            case "I03":
                return "意見欄";
            case "J01":
                return "常見問題";
            case "K01":
                return "聯絡我們";
            case "K02":
                return "地圖";
            case "L01":
                return "LOGO置換";
            case "b01":
                return "網站管理設定(英)";
            case "c01":
                return "首頁管理(英)";
            case "d01":
                return "經營理念(英)";
            case "d02":
                return "我們的優勢(英)";
            case "d03":
                return "組織概要(英)";
            case "d04":
                return "里程碑(英)";
            case "d05":
                return "客戶範圍(英)";
            case "e01":
                return "最新消息(英)";
            case "f01":
                return "品質認證(英)";
            case "f02":
                return "實驗室(英)";
            case "g01":
                return "產品介紹(英)";
            case "g02":
                return "產品維護(英)";
            case "h01":
                return "訓練與發展(英)";
            case "h02":
                return "人才招募(英)";
            case "h03":
                return "福利制度(英)";
            case "h04":
                return "活動花絮(英)";
            case "i01":
                return "下載資訊(英)";
            case "i02":
                return "資源下載(英)";
            case "i03":
                return "意見欄(英)";
            case "j01":
                return "常見問題(英)";
            case "k01":
                return "聯絡我們(英)";
            case "k02":
                return "地圖(英)";
            case "l01":
                return "LOGO置換(英)";
            default:
                return "無此模組";
        }
    }

    #endregion



}