﻿using System;
using System.Web;
using System.Data;
using System.IO;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Configuration;
using System.Web.Security;
using System.Text;


/// <summary>
/// DataLayer 的摘要描述
/// </summary>
public class DataLayer
{
    readonly OleDbConnection _connection;
    
    #region "建構子"
    /// <summary>
    /// 建構子，預設使用web.config裡連線字串名稱為ConnectionString
    /// </summary>
    public DataLayer()
    {
        _connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["AccessFile"].ToString()) + ";User Id=admin;Password=;");
    }
    /// <summary>
    /// 建構子，傳入SqlConnection物件
    /// </summary>
    /// <param name="conn">傳入SqlConnection物件</param>
    public DataLayer(OleDbConnection conn)
    {
        _connection = conn;
    }
    /// <summary>
    /// 建構子，傳入連線字串
    /// </summary>
    /// <param name="connectionString"></param>
    public DataLayer(string appSettingsName)
    {
        _connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings[appSettingsName].ToString()) + ";User Id=admin;Password=;");
    }
    #endregion

    #region "登入"
    /// <summary>
    /// 登入
    /// </summary>
    /// <param name="account">帳號</param>
    /// <param name="password">密碼</param>
    /// <returns></returns>
    public DataRow Login(string account, string password)
    {
        //password = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "MD5");

        string commandString = File.ReadAllText(HttpContext.Current.Server.MapPath("~/App_Code/SQL/Login.sql"));

        //實做連線和命令字串

        OleDbCommand OleCommand = new OleDbCommand(commandString, _connection);
        OleCommand.Parameters.Add("account", OleDbType.VarWChar, 50);
        OleCommand.Parameters.Add("password", OleDbType.VarWChar, 50);
        OleCommand.Parameters["account"].Value = account;
        OleCommand.Parameters["password"].Value = password;

        OleDbDataAdapter oleDataAdapter = new OleDbDataAdapter(OleCommand);
        DataTable dataTable = new DataTable();
        oleDataAdapter.Fill((dataTable));

        return dataTable.Rows.Count > 0 ? dataTable.Rows[0] : null;

    }
    #endregion

 #region "帳號列表"
    /// <summary>
    /// 帳號列表
    /// </summary>
    /// <returns></returns>
    public DataTable AccountList()
    {
        //password = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "MD5");

        string commandString = File.ReadAllText(HttpContext.Current.Server.MapPath("~/App_Code/SQL/AccountList.sql"));

        //實做連線和命令字串

        OleDbCommand oleCommand = new OleDbCommand(commandString, _connection);

        OleDbDataAdapter oleDataAdapter = new OleDbDataAdapter(oleCommand);
        DataTable dataTable = new DataTable();
        oleDataAdapter.Fill((dataTable));

        return dataTable;

    }
    #endregion

    #region "產品列表"

    /// <summary>
    /// 產品列表
    /// </summary>
    /// <param name="subject">主旨關鍵字</param>
    /// <param name="poster">發佈者</param>
    /// <param name="ClassItemPath">類別關鍵字</param>
    /// <returns></returns>
    public DataTable GetProducts(string subject, string poster, string ClassItemPath)
    {
        string commandString = File.ReadAllText(HttpContext.Current.Server.MapPath("~/App_Code/SQL/GetProducts.sql"));
        StringBuilder searchStringBuilder = new StringBuilder();
        OleDbCommand oleCommand = new OleDbCommand();
        if (!string.IsNullOrEmpty(subject))
        {
            searchStringBuilder.Append(" and ( Products.[subject] LIKE '%' + ? + '%')");
            oleCommand.Parameters.Add("subject", OleDbType.VarWChar, 50);
            oleCommand.Parameters["subject"].Value = subject;
        }
        if (!string.IsNullOrEmpty(poster))
        {
            searchStringBuilder.Append(" and ( Products.[poster] = ?)");
            oleCommand.Parameters.Add("poster", OleDbType.VarWChar, 50);
            oleCommand.Parameters["poster"].Value = poster;
        }
        if (!string.IsNullOrEmpty(ClassItemPath))
        {
            searchStringBuilder.Append(" and ( Products.[ClassItemPath] LIKE '%' + ? + '%')");
            oleCommand.Parameters.Add("ClassItemPath", OleDbType.VarWChar, 100);
            oleCommand.Parameters["ClassItemPath"].Value = ClassItemPath;
        }
       
        commandString = commandString.Replace("1=1", string.Format("1=1{0}{1}", Environment.NewLine, searchStringBuilder));
        oleCommand.Connection = _connection;
        oleCommand.CommandText = commandString + " order by Products.[initDate] desc";
        OleDbDataAdapter oleDataAdapter = new OleDbDataAdapter(oleCommand);
        DataTable dataTable = new DataTable();
        oleDataAdapter.Fill((dataTable));
        return dataTable;
    }
    #endregion

    #region "取得所有相片"

    /// <summary>
    /// 取得所有相片
    /// </summary>
    /// <param name="subject">主旨關鍵字</param>
    /// <param name="poster">發佈者</param>
    /// <param name="ClassItemPath">類別關鍵字</param>
    /// <returns></returns>
    public DataTable GetAllPic()
    {
        string commandString = File.ReadAllText(HttpContext.Current.Server.MapPath("~/App_Code/SQL/GetAllPic.sql"));
        StringBuilder searchStringBuilder = new StringBuilder();
        OleDbCommand oleCommand = new OleDbCommand();
        
        oleCommand.Connection = _connection;
        oleCommand.CommandText = commandString;
        OleDbDataAdapter oleDataAdapter = new OleDbDataAdapter(oleCommand);
        DataTable dataTable = new DataTable();
        oleDataAdapter.Fill((dataTable));
        return dataTable;
    }
    #endregion

    #region "首頁取得新聞四筆數"

    /// <summary>
    /// 首頁取得新聞四筆數
    /// </summary>
    /// <returns></returns>
    public DataTable GetTop4News(string version)
    {
        string commandString = File.ReadAllText(HttpContext.Current.Server.MapPath("~/App_Code/SQL/GetTop4News.sql"));
        StringBuilder searchStringBuilder = new StringBuilder();
        OleDbCommand oleCommand = new OleDbCommand();

        if (!string.IsNullOrEmpty(version))
        {
            searchStringBuilder.Append(" and News.[version] = ?");
            oleCommand.Parameters.Add("version", OleDbType.VarWChar, 100);
            oleCommand.Parameters["version"].Value = version;
        }

        commandString = commandString.Replace("1=1", string.Format("1=1 {0}", searchStringBuilder));
        oleCommand.Connection = _connection;
        oleCommand.CommandText = commandString + " order by News.[initDate] desc";
        OleDbDataAdapter oleDataAdapter = new OleDbDataAdapter(oleCommand);
        DataTable dataTable = new DataTable();
        oleDataAdapter.Fill((dataTable));
        return dataTable;
    }
    #endregion

    #region "消息列表"

    /// <summary>
    /// 消息列表
    /// </summary>
    /// <param name="version">語言版本</param>
    /// <param name="pageSize">分頁頁數</param>
    /// <param name="currentPage">當前頁面</param>
    /// <returns></returns>
    public DataTable GetNewsAllList(string version, int pageSize, int currentPage)
    {
        string commandString = File.ReadAllText(HttpContext.Current.Server.MapPath("~/App_Code/SQL/GetNewsAllList.sql"));
        //搜尋字串
        StringBuilder searchStringBuilder = new StringBuilder();
        OleDbCommand oleCommand = new OleDbCommand();

        if (!string.IsNullOrEmpty(version))
        {
            searchStringBuilder.Append(" and News.[version] = ?");
            
        }
        commandString = commandString.Replace("1=1", string.Format("1=1 {0}", searchStringBuilder));
        

        //處理參數
        oleCommand.Parameters.Add("version", OleDbType.VarWChar, 100);
        oleCommand.Parameters["version"].Value = version;

        //處理分頁
        oleCommand.Parameters.Add("start", OleDbType.Integer);
        oleCommand.Parameters["start"].Value = ((currentPage - 1) * pageSize) + 1;
        oleCommand.Parameters.Add("end", OleDbType.Integer);
        oleCommand.Parameters["end"].Value = currentPage * pageSize;

        oleCommand.Connection = _connection;
        oleCommand.CommandText = commandString + " order by News.[initDate] desc";
        OleDbDataAdapter oleDataAdapter = new OleDbDataAdapter(oleCommand);
        DataTable dataTable = new DataTable();
        oleDataAdapter.Fill((dataTable));
        return dataTable;
    }

    public int GetNewsAllListCount(string version)
    {

        string commandString = File.ReadAllText(HttpContext.Current.Server.MapPath("~/App_Code/SQL/GetNewsAllListCount.sql"));


        //搜尋字串
        StringBuilder searchStringBuilder = new StringBuilder();
        OleDbCommand oleCommand = new OleDbCommand();

        if (!string.IsNullOrEmpty(version))
        {
            searchStringBuilder.Append(" and News.[version] = ?");
            
        }
        commandString = commandString.Replace("1=1", string.Format("1=1 {0}",  searchStringBuilder));


        //處理參數
        oleCommand.Parameters.Add("version", OleDbType.VarWChar, 100);
        oleCommand.Parameters["version"].Value = version;

        oleCommand.Connection = _connection;
        oleCommand.CommandText = commandString;
        OleDbDataAdapter oleDataAdapter = new OleDbDataAdapter(oleCommand);
        DataTable dataTable = new DataTable();
        oleDataAdapter.Fill((dataTable));
        return dataTable.Rows.Count > 0 ? Convert.ToInt32(dataTable.Rows[0][0].ToString()) : 0;
    }
     #endregion
}
