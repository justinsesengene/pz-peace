﻿//Access版 2011/3/7 V0.3 bata
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;


/// <summary>
/// EasyDataProvide 的摘要描述
/// </summary>
public class EasyDataProvide
{
    private OleDbConnection _myConnection;
    private ArrayList _alSchemaName;//存放資料庫Schema名稱
    private ArrayList _alSchemaType;//存放資料庫Schema類型
    private ArrayList _alParameterSort;//存放資料庫Schema類型
    private StringDictionary _sdNameValues;//存放MAPP完畢的結果
    private readonly string _tableName;
    private bool _useShortDateFormate = true;
    private ArrayList _FileUploadSetups;
    public ArrayList FileUploadSetups
    {
        get
        {
            return _FileUploadSetups;
        }
        set
        {
            _FileUploadSetups = value;
        }
    }//存放檔案上傳設定;

    /*
        private string _filterString;
    */
    /// <summary>
    /// 是否在頁面使用短日期格式
    /// </summary>
    public bool useShortDateFormate
    {
        get { return _useShortDateFormate; }
        set { _useShortDateFormate = value; }

    }



    #region "建構子"
    /// <summary>
    /// 初始化類別,使用在web.config裡連線字串名稱為ConnectionString
    /// </summary>
    /// <param name="dataTableName">資料表名稱</param>
    public EasyDataProvide(string dataTableName)
    {
        OleDbConnection conn = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["AccessFile"].ToString()) + ";User Id=admin;Password=;");
        init(conn, dataTableName);
        _tableName = dataTableName;
    }
    /// <summary>
    /// 初始化類別,使用在web.config裡連線字串名稱自訂
    /// </summary>
    /// <param name="appSettingsName">web.config裡appSettings名稱</param>
    /// <param name="dataTableName">資料表名稱</param>
    public EasyDataProvide(string appSettingsName, string dataTableName)
    {
        OleDbConnection conn = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings[appSettingsName].ToString()) + ";User Id=admin;Password=;");
        init(conn, dataTableName);
        _tableName = dataTableName;
    }
    /// <summary>
    /// 初始化類別,丟入連線字串物件
    /// </summary>
    /// <param name="conn">丟入連線字串物件</param>
    /// <param name="dataTableName">資料表名稱</param>
    public EasyDataProvide(OleDbConnection conn, string dataTableName)
    {
        init(conn, dataTableName);
        _tableName = dataTableName;
    }

    private void init(OleDbConnection conn, string dataTableName)
    {
        _myConnection = conn;
        //取得table 欄位名稱
        string schemaString = String.Format("SELECT top 1 * from {0}", dataTableName);

        OleDbCommand myCommand = new OleDbCommand(schemaString, conn);
        _alSchemaName = new ArrayList();
        _alSchemaType = new ArrayList();
        _alParameterSort = new ArrayList();
        _sdNameValues = new StringDictionary();
        OleDbDataAdapter dataAdapter = new OleDbDataAdapter(myCommand);
        DataTable dt = new DataTable();
        dataAdapter.Fill(dt);
        foreach (DataColumn col in dt.Columns)
        {
            _alSchemaName.Add(col.ColumnName);
            _alSchemaType.Add(col.DataType.Name);
        }

        FileUploadSetups = new ArrayList();

    }

    #endregion

    #region "將頁面上的互動控制項的值直接送到參數集"
    /// <summary>
    /// 將頁面上的互動控制項的值直接送到參數集合，checkbox與Upload 控制項除外
    /// </summary>
    /// <param name="page">傳入Page就對了!!</param>
    public void setPageFormQuest(Page page)
    {
        setControlQuest(page);



    }

    public void setPageFormQuest(Page page, string contentPlaceHolderName)
    {
        ContentPlaceHolder contentPlaceHolder = (ContentPlaceHolder)page.Master.FindControl(contentPlaceHolderName);
        setControlQuest(contentPlaceHolder);
    }

    private void setControlQuest(Control container)
    {
        //取得頁面控制項
        string dataValue = "";
        bool flag = true;
        foreach (string name in _alSchemaName)
        {
            Control oj = container.FindControl(name);
            if (oj != null)
            {
                if (oj is TextBox)
                {
                    TextBox textBox = (TextBox)oj;
                    dataValue = textBox.Text;
                }
                if (oj is CheckBox)
                {
                    CheckBox checkBox = (CheckBox)oj;
                    dataValue = Convert.ToString(checkBox.Checked);
                }

                if (oj is RadioButton)
                {
                    RadioButton radioButton = (RadioButton)oj;
                    dataValue = Convert.ToString(radioButton.Checked);
                }

                if (oj is HiddenField)
                {
                    HiddenField hiddenField = (HiddenField)oj;
                    dataValue = hiddenField.Value;
                }
                if (oj is DropDownList)
                {
                    DropDownList downList = (DropDownList)oj;
                    if (downList.Items.Count > 0)
                    {
                        dataValue = downList.SelectedValue;
                    }
                }
                if (oj is HtmlInputControl)
                {
                    HtmlInputControl htmlInputText = (HtmlInputControl)oj;
                    dataValue = htmlInputText.Value;
                }
                if (oj is FileUpload)
                {
                    string savePath = ConfigurationManager.AppSettings["FileUploadPath"];
                    if (savePath.IndexOf("\\") == -1) //相對位址
                    {
                        savePath = System.Web.HttpContext.Current.Server.MapPath(savePath);
                    }
                    int i = 0;
                    foreach (FileUploadSetup fus in FileUploadSetups)
                    {
                        i++;
                        if (name == fus.name)
                        {
                            FileUpload fu = (FileUpload)oj;
                            if (fu.HasFile)
                            {
                                //取得副檔名
                                string Extension = fu.FileName.Split('.')[fu.FileName.Split('.').Length - 1];
                                //新檔案名稱
                                string NewfileName = String.Format("{0:yyyyMMddhhmmsss}-{1}.{2}", DateTime.Now, i, Extension);
                                dataValue = NewfileName;

                                //若是有單獨設定
                                if (!string.IsNullOrEmpty(fus.SavePath))
                                {
                                    savePath = fus.SavePath;
                                    if (savePath.IndexOf("\\") == -1) //相對位址
                                    {
                                        savePath = System.Web.HttpContext.Current.Server.MapPath(savePath);
                                    }
                                }

                                if (fus.fileType == FileUploadSetup.UpfileType.Image) //圖片上傳
                                {
                                    //檢查檔案格式
                                    if (fu.PostedFile.ContentType.IndexOf("image") == -1)
                                    {
                                        throw new ArgumentException("請檢查上傳檔案格式");
                                    }
                                    savePath += "\\Images\\";
                                    //處理縮圖
                                    if (fus.ThumbnailImages.Count > 0)
                                    {
                                        foreach (ThumbnailImage img in fus.ThumbnailImages)
                                        {
                                            GenerateThumbnailImage(NewfileName, fu.PostedFile.InputStream, savePath, img.suffix, img.maxWidth,
                                                                   img.MaxHight);
                                        }
                                    }

                                }
                                else
                                {
                                    savePath += "\\Files\\";
                                }




                                fu.SaveAs(savePath + NewfileName);

                            }
                            else
                            {
                                flag = false;
                                if (fus.allowNoFile == false)
                                {
                                    throw new ArgumentException("請上傳檔案!");
                                }
                            }
                        }
                    }

                }
                if (flag)
                {
                    _sdNameValues.Add(name, dataValue);
                    _alParameterSort.Add(name);
                }



            }
        }
    }

    #endregion

    #region "新增欄位名稱與值到參數集合"
    /// <summary>
    /// 新增欄位名稱與值到參數集合，若是key已經存在會更新value
    /// </summary>
    /// <param name="key">欄位名稱</param>
    /// <param name="value">欄位的值</param>
    public void addParameter(string key, string value)
    {
        if (_sdNameValues.ContainsKey(key))
        {
            _sdNameValues[key] = value;
        }
        else
        {
            _sdNameValues.Add(key, value);
            _alParameterSort.Add(key);
        }
    }

    #endregion

    #region "新增欄位名稱與值到參數集合"
    public void addParameterSort(string value)
    {
        _alParameterSort.Add(value);
    }

    #endregion

    #region "移除欄位名稱與值到參數集合"
    /// <summary>
    /// 移除欄位名稱與值到參數集合
    /// </summary>
    /// <param name="key">欄位名稱</param>
    public void removeParameter(string key)
    {
        if (_sdNameValues.ContainsKey(key))
        {
            _sdNameValues.Remove(key);
        }
    }

    #endregion

    #region "顯示參數集合內容"
    /// <summary>
    /// 顯示參數集合內容
    /// </summary>
    /// <returns>回傳參數集合內容</returns>
    public string showParameters()
    {
        StringBuilder parametersStringBuilder = new StringBuilder();
        foreach (string SchemaName in _alParameterSort)
        {
            parametersStringBuilder.Append(String.Format("{0}:{1}<br />", SchemaName, _sdNameValues[SchemaName]));

        }
        return parametersStringBuilder.ToString();
    }

    #endregion

    #region "取得一筆資料(SQL 語法 where 後面接的字串)"
    /// <summary>
    /// 取得一筆資料,要自行處理sql injection
    /// </summary>
    /// <param name="filterString">(SQL 語法 where 後面接的字串)</param>
    /// <returns>回傳dataReader 一筆記錄</returns>
    public DataRow getSingleRow(string filterString)
    {
        string selectString = String.Format("select * from {0} where {1}", _tableName, filterString);
        OleDbCommand oleCommand = new OleDbCommand(selectString, _myConnection);
        int i = 0;
        foreach (string columnName in _alSchemaName)
        {
            if (_sdNameValues.ContainsKey(columnName))
            {
                oleCommand.Parameters.Add(columnName.Replace(" ", "_"), getDATA_TYPE(Convert.ToString(_alSchemaType[i])));
                if (Convert.ToString(_alSchemaType[i]) == "Boolean")
                {
                    oleCommand.Parameters[columnName].Value = _sdNameValues[columnName].ToLower() == "true" ? "1" : "0";
                }
                else
                {
                    oleCommand.Parameters[columnName].Value = _sdNameValues[columnName];
                }

            }
            i++;
        }

        OleDbDataAdapter oleDataAdapter = new OleDbDataAdapter(oleCommand);
        DataTable dataTable = new DataTable();
        oleDataAdapter.Fill(dataTable);
        DataRow dataRow = dataTable.Rows.Count > 0 ? dataTable.Rows[0] : null;

        return dataRow;
    }

    #endregion

    #region "根據id取得一筆資料"
    /// <summary>
    /// 根據id取得一筆資料
    /// </summary>
    /// <param name="id">輸入id(資料庫的id欄位須為主索引)</param>
    /// <returns>回傳dataReader 一筆記錄</returns>
    public DataRow getByID(string id)
    {
        string selectString = String.Format("select * from {0} where ID= ?", _tableName);
        OleDbCommand oleCommand = new OleDbCommand(selectString, _myConnection);
        int i = _alSchemaName.IndexOf("id");
        oleCommand.Parameters.Add("id", getDATA_TYPE(Convert.ToString(_alSchemaType[i])));
        oleCommand.Parameters["id"].Value = id;
        OleDbDataAdapter oleDataAdapter = new OleDbDataAdapter(oleCommand);
        DataTable dataTable = new DataTable();
        oleDataAdapter.Fill(dataTable);
        DataRow dataRow = dataTable.Rows.Count <= 0 ? null : dataTable.Rows[0];
        return dataRow;
    }
    #endregion

    #region "取得所有資料"
    /// <summary>
    /// 取得所有資料
    /// </summary>
    /// <returns>DataTable</returns>
    public DataTable getAllData()
    {
        string selectString = String.Format("select * from {0}", _tableName);
        OleDbCommand oleCommand = new OleDbCommand(selectString, _myConnection);
        OleDbDataAdapter oleDataAdapter = new OleDbDataAdapter(oleCommand);
        DataTable dataTable = new DataTable();
        oleDataAdapter.Fill(dataTable);
        return dataTable;
    }

    /// <summary>
    /// 取得所有資料
    /// </summary>
    /// <param name="orderbyString">排序字串 (例:order by id asc)</param>
    /// <returns>DataTable</returns>
    public DataTable getAllData(string orderbyString)
    {
        string selectString = String.Format("select * from {0} {1}", _tableName, orderbyString);
        OleDbCommand oleCommand = new OleDbCommand(selectString, _myConnection);
        OleDbDataAdapter oleDataAdapter = new OleDbDataAdapter(oleCommand);
        DataTable dataTable = new DataTable();
        oleDataAdapter.Fill(dataTable);
        return dataTable;
    }
    #endregion

    #region "依照條件傳回資料表資料"
    /// <summary>
    /// 依照條件傳回資料表資料
    /// </summary>
    /// <param name="filterString">SQL 語法 where 後面接的字串 (例:id=?)</param>
    /// <returns>DataTable</returns>
    public DataTable getData(string filterString)
    {
        string selectString = String.Format("select * from {0} where {1}", _tableName, filterString);
        OleDbCommand oleCommand = new OleDbCommand(selectString, _myConnection);
        int i = 0;
        foreach (string columnName in _alSchemaName)
        {
            if (_sdNameValues.ContainsKey(columnName))
            {
                oleCommand.Parameters.Add(columnName.Replace(" ", "_"), getDATA_TYPE(Convert.ToString(_alSchemaType[i])));
                if (Convert.ToString(_alSchemaType[i]) == "Boolean")
                {
                    oleCommand.Parameters[columnName].Value = _sdNameValues[columnName].ToLower() == "true" ? "1" : "0";
                }
                else
                {
                    oleCommand.Parameters[columnName].Value = _sdNameValues[columnName];
                }
            }
            i++;
        }

        OleDbDataAdapter oleDataAdapter = new OleDbDataAdapter(oleCommand);
        DataTable dataTable = new DataTable();
        oleDataAdapter.Fill(dataTable);
        return dataTable;
    }
    /// <summary>
    /// 依照條件傳回資料表資料
    /// </summary>
    /// <param name="filterString">SQL 語法 where 後面接的字串 (例:id=?)</param>
    /// <param name="orderbyString">排序字串 (例:order by id asc)</param>
    /// <returns></returns>
    public DataTable getData(string filterString, string orderbyString)
    {
        string selectString = String.Format("select * from {0} where {1} {2}", _tableName, filterString, orderbyString);
        OleDbCommand oleCommand = new OleDbCommand(selectString, _myConnection);
        int i = 0;
        foreach (string columnName in _alSchemaName)
        {
            if (_sdNameValues.ContainsKey(columnName))
            {
                oleCommand.Parameters.Add(columnName.Replace(" ", "_"), getDATA_TYPE(Convert.ToString(_alSchemaType[i])));
                if (Convert.ToString(_alSchemaType[i]) == "Boolean")
                {
                    oleCommand.Parameters[columnName].Value = _sdNameValues[columnName].ToLower() == "true" ? "1" : "0";
                }
                else
                {
                    oleCommand.Parameters[columnName].Value = _sdNameValues[columnName];
                }
            }
            i++;
        }
        string Parameter = "";

        for (int p = 0; p <= oleCommand.Parameters.Count - 1; p++)
        {
            Parameter += oleCommand.Parameters[p].ParameterName + ":" + oleCommand.Parameters[p].Value + "<br />";
        }
        OleDbDataAdapter oleDataAdapter = new OleDbDataAdapter(oleCommand);
        DataTable dataTable = new DataTable();
        oleDataAdapter.Fill(dataTable);
        return dataTable;
    }

    /// <summary>
    /// 依照條件傳回資料表資料
    /// </summary>
    /// <param name="topCount">抓幾筆</param>
    /// <param name="filterString">SQL 語法 where 後面接的字串 (例:id=?)</param>
    /// <returns>DataTable</returns>
    public DataTable getData(int topCount, string filterString)
    {
        string selectString = String.Format("select top {2} * from {0} where {1}", _tableName, filterString,topCount);
        OleDbCommand oleCommand = new OleDbCommand(selectString, _myConnection);
        int i = 0;
        foreach (string columnName in _alSchemaName)
        {
            if (_sdNameValues.ContainsKey(columnName))
            {
                oleCommand.Parameters.Add(columnName.Replace(" ", "_"), getDATA_TYPE(Convert.ToString(_alSchemaType[i])));
                if (Convert.ToString(_alSchemaType[i]) == "Boolean")
                {
                    oleCommand.Parameters[columnName].Value = _sdNameValues[columnName].ToLower() == "true" ? "1" : "0";
                }
                else
                {
                    oleCommand.Parameters[columnName].Value = _sdNameValues[columnName];
                }
            }
            i++;
        }

        OleDbDataAdapter oleDataAdapter = new OleDbDataAdapter(oleCommand);
        DataTable dataTable = new DataTable();
        oleDataAdapter.Fill(dataTable);
        return dataTable;
    }
    /// <summary>
    /// 依照條件傳回資料表資料
    /// </summary>
    /// <param name="filterString">SQL 語法 where 後面接的字串 (例:id=?)</param>
    /// <param name="orderbyString">排序字串 (例:order by id asc)</param>
    /// <returns></returns>
    public DataTable getData(int topCount,string filterString, string orderbyString)
    {
        string selectString = String.Format("select top {3} * from {0} where {1} {2}", _tableName, filterString, orderbyString,topCount);
        OleDbCommand oleCommand = new OleDbCommand(selectString, _myConnection);
        int i = 0;
        foreach (string columnName in _alSchemaName)
        {
            if (_sdNameValues.ContainsKey(columnName))
            {
                oleCommand.Parameters.Add(columnName.Replace(" ", "_"), getDATA_TYPE(Convert.ToString(_alSchemaType[i])));
                if (Convert.ToString(_alSchemaType[i]) == "Boolean")
                {
                    oleCommand.Parameters[columnName].Value = _sdNameValues[columnName].ToLower() == "true" ? "1" : "0";
                }
                else
                {
                    oleCommand.Parameters[columnName].Value = _sdNameValues[columnName];
                }
            }
            i++;
        }
        string Parameter = "";

        for (int p = 0; p <= oleCommand.Parameters.Count - 1; p++)
        {
            Parameter += oleCommand.Parameters[p].ParameterName + ":" + oleCommand.Parameters[p].Value + "<br />";
        }
        OleDbDataAdapter oleDataAdapter = new OleDbDataAdapter(oleCommand);
        DataTable dataTable = new DataTable();
        oleDataAdapter.Fill(dataTable);
        return dataTable;
    }
    #endregion





    #region "依照ID取得一筆資料 填入頁面控制項"
    /// <summary>
    /// 依照ID取得一筆資料填入頁面控制項(適用一般頁面)
    /// </summary>
    /// <param name="id">輸入id(資料庫的id欄位須為主索引)</param>
    /// <param name="page">填入Page或this都可以</param>
    /// <returns>回傳搜尋結果</returns>
    public DataRow fillPageControlsByID(string id, Page page)
    {
        DataRow dataRow = fillData(id, page, true);
        return dataRow;
    }
    /// <summary>
    /// 依照ID取得一筆資料填入頁面控制項(適用MasterPage)
    /// </summary>
    /// <param name="id">輸入id(資料庫的id欄位須為主索引)</param>
    /// <param name="page">填入Page或this都可以</param>
    /// <param name="contentPlaceHolderName">Master裡ContentPlaceHolder 名稱,預設是ContentPlaceHolder1</param>
    /// <returns>回傳搜尋結果</returns>
    public DataRow fillPageControlsByID(string id, Page page, string contentPlaceHolderName)
    {

        ContentPlaceHolder contentPlaceHolder = (ContentPlaceHolder)page.Master.FindControl(contentPlaceHolderName);
        DataRow dataRow = fillData(id, contentPlaceHolder, true);
        return dataRow;
    }

    /// <summary>
    /// 依照ID取得一筆資料填入頁面控制項(適用於其它容器控制項如UserControl或者Panel)
    /// </summary>
    /// <param name="id">輸入id(資料庫的id欄位須為主索引)</param>
    /// <param name="control">容器控制項如UserControl或者Panel</param>
    /// <returns>回傳搜尋結果</returns>
    public DataRow fillPageControlsByID(string id, Control control)
    {
        DataRow dataRow = fillData(id, control, true);
        return dataRow;
    }



    #endregion

    #region "取得一筆資料 填入頁面控制項"
    /// <summary>
    /// 取得一筆資料填入頁面控制項(適用一般頁面),要自行處理sql injection
    /// </summary>
    /// <param name="filterString">SQL 語法 where 後面接的字串 (例:id=?)</param>
    /// <param name="page">填入Page或this都可以</param>
    /// <returns>回傳搜尋結果</returns>
    public DataRow fillPageControls(string filterString, Page page)
    {
        DataRow dataRow = fillData(filterString, page, false);
        return dataRow;
    }

    /// <summary>
    /// 取得一筆資料填入頁面控制項(適用MasterPage),要自行處理sql injection
    /// </summary>
    /// <param name="filterString">SQL 語法 where 後面接的字串 (例:id=?)</param>
    /// <param name="page">填入Page或this都可以</param>
    /// <param name="contentPlaceHolderName">Master裡ContentPlaceHolder 名稱</param>
    /// <returns>回傳搜尋結果</returns>
    public DataRow fillPageControls(string filterString, Page page, string contentPlaceHolderName)
    {

        ContentPlaceHolder contentPlaceHolder = (ContentPlaceHolder)page.Master.FindControl(contentPlaceHolderName);
        DataRow dataRow = fillData(filterString, contentPlaceHolder, false);
        return dataRow;
    }

    /// <summary>
    /// 取得一筆資料填入頁面控制項(適用於其它容器控制項如UserControl或者Panel),要自行處理sql injection
    /// </summary>
    /// <param name="filterString">SQL 語法 where 後面接的字串</param>
    /// <param name="control">容器控制項如UserControl或者Panel</param>
    /// <returns>回傳搜尋結果</returns>
    public DataRow fillPageControls(string filterString, Control control)
    {
        DataRow dataRow = fillData(filterString, control, false);
        return dataRow;
    }

    private DataRow fillData(string filterString, Control page, Boolean isId)
    {
        //取得資料
        string selectString;
        OleDbCommand oleCommand;

        switch (isId)
        {
            case true:
                {
                    selectString = String.Format("select * from {0} where ID=?", _tableName);
                    oleCommand = new OleDbCommand(selectString, _myConnection);
                    int i = _alSchemaName.IndexOf("id");
                    oleCommand.Parameters.Add("id", getDATA_TYPE(Convert.ToString(_alSchemaType[i])));
                    oleCommand.Parameters["id"].Value = filterString;

                }
                break;
            default:
                {
                    selectString = String.Format("select * from {0} where {1}", _tableName, filterString);
                    oleCommand = new OleDbCommand(selectString, _myConnection);
                    int i = 0;
                    foreach (string columnName in _alSchemaName)
                    {
                        if (_sdNameValues.ContainsKey(columnName))
                        {
                            oleCommand.Parameters.Add(columnName.Replace(" ", "_"), getDATA_TYPE(Convert.ToString(_alSchemaType[i])));
                            if (Convert.ToString(_alSchemaType[i]) == "Boolean")
                            {
                                oleCommand.Parameters[columnName].Value = _sdNameValues[columnName].ToLower() == "true" ? "1" : "0";
                            }
                            else
                            {
                                oleCommand.Parameters[columnName].Value = _sdNameValues[columnName];
                            }
                        }
                        i++;
                    }
                }
                break;
        }


        OleDbDataAdapter oleDataAdapter = new OleDbDataAdapter(oleCommand);
        DataTable dataTable = new DataTable();
        oleDataAdapter.Fill(dataTable);
        DataRow dataRow;
        if (dataTable.Rows.Count > 0)
        {
            //取得頁面控制項
            dataRow = dataTable.Rows[0];
            int i = 0;
            foreach (string name in _alSchemaName)
            {
                Control oj = page.FindControl(name);
                if (oj != null)
                {
                    if (oj is TextBox)
                    {
                        TextBox textBox = (TextBox)oj;
                        textBox.Text = ConvertData(dataRow[name], i);

                    }
                    if (oj is Label)
                    {
                        Label label = (Label)oj;
                        label.Text = ConvertData(dataRow[name], i);
                    }
                    if (oj is Button)
                    {
                        Button button = (Button)oj;
                        button.Text = ConvertData(dataRow[name], i);
                    }
                    if (oj is Literal)
                    {
                        Literal literal = (Literal)oj;
                        literal.Text = ConvertData(dataRow[name], i);
                    }
                    if (oj is HyperLink)
                    {
                        HyperLink hyperLink = (HyperLink)oj;
                        hyperLink.Text = ConvertData(dataRow[name], i);
                    }
                    if (oj is CheckBox)
                    {
                        CheckBox checkBox = (CheckBox)oj;
                        checkBox.Text = ConvertData(dataRow[name], i);
                    }

                    if (oj is RadioButton)
                    {
                        RadioButton radioButton = (RadioButton)oj;
                        radioButton.Text = ConvertData(dataRow[name], i);
                    }

                    if (oj is HiddenField)
                    {
                        HiddenField hiddenField = (HiddenField)oj;
                        hiddenField.Value = ConvertData(dataRow[name], i);
                    }
                    if (oj is DropDownList)
                    {
                        DropDownList downList = (DropDownList)oj;
                        if (downList.Items.Count > 0)
                        {
                            downList.SelectedValue = ConvertData(dataRow[name], i);
                        }
                    }
                    if (oj is HtmlInputControl)
                    {
                        HtmlInputControl htmlInputText = (HtmlInputControl)oj;
                        htmlInputText.Value = ConvertData(dataRow[name], i);
                    }


                }
                i++;
            }
        }
        else
        {
            dataRow = null;
        }





        return dataRow;
    }


    #endregion

    #region "執行新增"
    /// <summary>
    /// 執行新增命令
    /// </summary>
    public void Insert()
    {
        StringBuilder sbCommand = new StringBuilder(String.Format("INSERT INTO [{0}] (", _tableName));
        StringBuilder sbColumns = new StringBuilder();
        StringBuilder sbParameters = new StringBuilder();
        OleDbCommand oleCommand = new OleDbCommand();
        int i = 0;
        foreach (string columnName in _alSchemaName)
        {
            if (_sdNameValues.ContainsKey(columnName) && (!string.IsNullOrEmpty(_sdNameValues[columnName])))
            {
                sbColumns.Append(String.Format("[{0}],", columnName));
                sbParameters.Append("?,");
                oleCommand.Parameters.Add(columnName.Replace(" ", "_"), getDATA_TYPE(Convert.ToString(_alSchemaType[i])));
                if (Convert.ToString(_alSchemaType[i]) == "Boolean")
                {
                    oleCommand.Parameters[columnName].Value = _sdNameValues[columnName].ToLower() == "true" ? "1" : "0";
                }
                else
                {
                    oleCommand.Parameters[columnName].Value = _sdNameValues[columnName];
                }
            }
            i++;
        }
        if (sbColumns.ToString() == "")
        {
            return;
        }
        string Parameter = "";

        for (int p = 0; p <= oleCommand.Parameters.Count - 1; p++)
        {
            Parameter += oleCommand.Parameters[p].ParameterName + ":" + oleCommand.Parameters[p].Value + "<br />";
        }
        sbCommand.Append(String.Format("{0})VALUES ({1})", sbColumns.ToString().Trim(','), sbParameters.ToString().Trim(',')));
        oleCommand.CommandText = sbCommand.ToString();
        oleCommand.Connection = _myConnection;
        _myConnection.Open();
        oleCommand.ExecuteNonQuery();
        _myConnection.Close();

    }
    public int InsertReturnValue()
    {
        StringBuilder sbCommand = new StringBuilder(String.Format("INSERT INTO [{0}] (", _tableName));
        StringBuilder sbColumns = new StringBuilder();
        StringBuilder sbParameters = new StringBuilder();
        OleDbCommand oleCommand = new OleDbCommand();
        int i = 0;
        foreach (string columnName in _alSchemaName)
        {
            if (_sdNameValues.ContainsKey(columnName) && (!string.IsNullOrEmpty(_sdNameValues[columnName])))
            {
                sbColumns.Append(String.Format("[{0}],", columnName));
                sbParameters.Append("?,");
                oleCommand.Parameters.Add(columnName.Replace(" ", "_"), getDATA_TYPE(Convert.ToString(_alSchemaType[i])));
                if (Convert.ToString(_alSchemaType[i]) == "Boolean")
                {
                    oleCommand.Parameters[columnName].Value = _sdNameValues[columnName].ToLower() == "true" ? "1" : "0";
                }
                else
                {
                    oleCommand.Parameters[columnName].Value = _sdNameValues[columnName];
                }
            }
            i++;
        }
        if (sbColumns.ToString() == "")
        {
            return 0;
        }
        string Parameter = "";

        for (int p = 0; p <= oleCommand.Parameters.Count - 1; p++)
        {
            Parameter += oleCommand.Parameters[p].ParameterName + ":" + oleCommand.Parameters[p].Value + "<br />";
        }
        sbCommand.Append(String.Format("{0})VALUES ({1})", sbColumns.ToString().Trim(','), sbParameters.ToString().Trim(',')));
        oleCommand.CommandText = sbCommand.ToString();
        oleCommand.Connection = _myConnection;
        _myConnection.Open();
        oleCommand.ExecuteNonQuery();
        oleCommand.CommandText = "SELECT @@IDENTITY";
        int n = (int)oleCommand.ExecuteScalar();

        _myConnection.Close();
        return n;

    }
    #endregion



    #region "執行修改"
    /// <summary>
    /// 執行修改命令,要自行處理sql injection
    /// </summary>
    /// <param name="filterString">SQL 語法 where 後面接的字串,(例:id=?)</param>
    public void Update(string filterString)
    {
        StringBuilder strcommBuilder = new StringBuilder("UPDATE [" + _tableName + "] set ");
        OleDbCommand oleCommand = new OleDbCommand();
        int i = 0;
        foreach (string columnName in _alSchemaName)
        {
            if (_sdNameValues.ContainsKey(columnName))
            {
                strcommBuilder.Append(String.Format("[{0}]=?,", columnName));
            }


        }
        foreach (string columnName in _alParameterSort)
        {
            if (_sdNameValues.ContainsKey(columnName))
            {
                int j = _alSchemaName.IndexOf(columnName);
                string strType = "String";
                if (j != -1)
                {
                    strType = Convert.ToString(_alSchemaType[j]);
                }
                oleCommand.Parameters.Add(columnName.Replace(" ", "_"), getDATA_TYPE(strType));
                if (strType == "Boolean")
                {
                    oleCommand.Parameters[columnName].Value = _sdNameValues[columnName].ToLower() == "true" ? "1" : "0";
                }
                else if (_sdNameValues[columnName] == "")
                {
                    oleCommand.Parameters[columnName].Value = DBNull.Value;
                }
                else
                {
                    oleCommand.Parameters[columnName].Value = _sdNameValues[columnName];
                }
            }
            i++;
        }
        if (oleCommand.Parameters.Count == 0)
        {
            return;
        }
        string sqlComm = strcommBuilder.ToString().Trim(',') + " ";
        if (!string.IsNullOrEmpty(filterString))
        {
            sqlComm += "where " + filterString;
        }
        string Parameter = "";

        for (int p = 0; p <= oleCommand.Parameters.Count - 1; p++)
        {
            Parameter += oleCommand.Parameters[p].ParameterName + ":" + oleCommand.Parameters[p].Value + "<br />";
        }

        oleCommand.CommandText = sqlComm;
        oleCommand.Connection = _myConnection;
        _myConnection.Open();
        oleCommand.ExecuteNonQuery();
        _myConnection.Close();


    }

    /// <summary>
    /// 依照ID執行修改命令
    /// </summary>
    /// <param name="id">輸入id(資料庫的id欄位須為主索引)</param>
    public void UpdateById(string id)
    {
        StringBuilder strcommBuilder = new StringBuilder("UPDATE [" + _tableName + "] set ");
        OleDbCommand oleCommand = new OleDbCommand();
        int i = 0;
        foreach (string columnName in _alSchemaName)
        {
            if (_sdNameValues.ContainsKey(columnName))
            {
                strcommBuilder.Append(String.Format("[{0}]=?,", columnName));
                oleCommand.Parameters.Add(columnName.Replace(" ", "_"), getDATA_TYPE(Convert.ToString(_alSchemaType[i])));
                if (Convert.ToString(_alSchemaType[i]) == "Boolean")
                {
                    oleCommand.Parameters[columnName].Value = _sdNameValues[columnName].ToLower() == "true" ? "1" : "0";
                }
                else if (_sdNameValues[columnName] == "")
                {
                    oleCommand.Parameters[columnName].Value = DBNull.Value;
                }
                else
                {
                    oleCommand.Parameters[columnName].Value = _sdNameValues[columnName];
                }

            }

            i++;
        }

        i = _alSchemaName.IndexOf("id");
        oleCommand.Parameters.Add("id", getDATA_TYPE(Convert.ToString(_alSchemaType[i])));
        oleCommand.Parameters["id"].Value = id;

        string sqlComm = strcommBuilder.ToString().Trim(',') + " ";
        sqlComm += "where [id]=?";

        oleCommand.CommandText = sqlComm;
        oleCommand.Connection = _myConnection;
        _myConnection.Open();
        oleCommand.ExecuteNonQuery();
        _myConnection.Close();


    }
    #endregion

    #region "執行刪除"
    /// <summary>
    /// 要自行處理sql injection
    /// </summary>
    /// <param name="filterString">SQL 語法 where 後面接的字串,(例:id=?)</param>
    public void Delete(string filterString)
    {
        OleDbCommand oleCommand = new OleDbCommand();
        string sqlComm = "DELETE FROM " + _tableName + " ";
        if (!string.IsNullOrEmpty(filterString))
        {
            sqlComm += "where " + filterString;
        }
        oleCommand.CommandText = sqlComm;
        oleCommand.Connection = _myConnection;
        int i = 0;
        foreach (string columnName in _alParameterSort)
        {
            if (_sdNameValues.ContainsKey(columnName))
            {
                int j = _alSchemaName.IndexOf(columnName);
                string strType = "String";
                if (j != -1)
                {
                    strType = Convert.ToString(_alSchemaType[j]);
                }
                oleCommand.Parameters.Add(columnName.Replace(" ", "_"), getDATA_TYPE(strType));
                if (strType == "Boolean")
                {
                    oleCommand.Parameters[columnName].Value = _sdNameValues[columnName].ToLower() == "true" ? "1" : "0";
                }
                else if (_sdNameValues[columnName] == "")
                {
                    oleCommand.Parameters[columnName].Value = DBNull.Value;
                }
                else
                {
                    oleCommand.Parameters[columnName].Value = _sdNameValues[columnName];
                }
            }
            i++;
        }


        _myConnection.Open();
        oleCommand.ExecuteNonQuery();
        _myConnection.Close();
    }
    /// <summary>
    /// 依照ID,執行刪除
    /// </summary>
    /// <param name="id">輸入id(資料庫的id欄位須為主索引)</param>
    public void DeleteById(string id)
    {
        OleDbCommand oleCommand = new OleDbCommand();
        string sqlComm = "DELETE FROM " + _tableName + " where id=?";

        int i = _alSchemaName.IndexOf("id");
        oleCommand.Parameters.Add("id", getDATA_TYPE(Convert.ToString(_alSchemaType[i])));
        oleCommand.Parameters["id"].Value = id;

        oleCommand.CommandText = sqlComm;
        oleCommand.Connection = _myConnection;
        _myConnection.Open();
        oleCommand.ExecuteNonQuery();
        _myConnection.Close();
    }


    #endregion

    #region "型態對應轉換"

    private static OleDbType getDATA_TYPE(string dataType)
    {
        switch (dataType.ToLower())
        {
            case "int32":
                return OleDbType.Integer;

            case "string":
                return OleDbType.VarWChar;

            case "dateTime":

                return OleDbType.Date;
            case "decimal":
                return OleDbType.Numeric;
            case "boolean":
                return OleDbType.Boolean;
            default:
                return OleDbType.VarWChar;
        }

    }

    #endregion


    #region "轉換資料，加上日期顯示判斷"

    /// <summary>
    /// 轉換資料，加上日期顯示判斷
    /// </summary>
    /// <param name="data">輸入要轉換的資料</param>
    /// <param name="i">第幾個columns</param>
    /// <returns></returns>
    private string ConvertData(object data, int i)
    {

        if (_useShortDateFormate)
        {
            DateTime result;
            if (Convert.ToString(_alSchemaType[i]).IndexOf("DateTime") != -1 && DateTime.TryParse(data.ToString(), out result))
            {
                return Convert.ToDateTime(data).ToString("d");
            }
        }


        return data.ToString();
    }
    #endregion

    /// <summary>
    /// 舉世無敵縮圖程式(多載)
    /// 1.會自動判斷是比較高還是比較寬，以比較大的那一方決定要縮的尺寸
    /// 2.指定寬度，等比例縮小
    /// 3.指定高度，等比例縮小
    /// </summary>
    /// <param name="name">原檔檔名</param>
    /// <param name="source">來源檔案的Stream,可接受上傳檔案</param>
    /// <param name="target">目的路徑</param>
    /// <param name="suffix">縮圖辯識符號</param>
    /// <param name="MaxWidth">指定要縮的寬度</param>
    /// <param name="MaxHight">指定要縮的高度</param>
    /// <remarks></remarks>
    static void GenerateThumbnailImage(string name, System.IO.Stream source, string target, string suffix, int MaxWidth, int MaxHight)
    {
        System.Drawing.Image baseImage = System.Drawing.Image.FromStream(source);
        Single ratio = 0.0F; //存放縮圖比例
        Single h = baseImage.Height; //圖像原尺寸高度
        Single w = baseImage.Width;  //圖像原尺寸寬度
        int ht; //圖像縮圖後高度
        int wt;//圖像縮圖後寬度
        if (w > h)
        {
            ratio = MaxWidth / w; //計算寬度縮圖比例
            if (MaxWidth < w)
            {
                ht = Convert.ToInt32(ratio * h);
                wt = MaxWidth;

            }
            else
            {
                ht = Convert.ToInt32(baseImage.Height);
                wt = Convert.ToInt32(baseImage.Width);

            }
        }
        else
        {
            ratio = MaxHight / h; //計算寬度縮圖比例
            if (MaxHight < h)
            {
                ht = MaxHight;
                wt = Convert.ToInt32(ratio * w);
            }
            else
            {
                ht = Convert.ToInt32(baseImage.Height);
                wt = Convert.ToInt32(baseImage.Width);
            }
        }
        string Newname = target + "\\" + suffix + name;
        System.Drawing.Bitmap img = new System.Drawing.Bitmap(wt, ht);
        System.Drawing.Graphics graphic = Graphics.FromImage(img);
        graphic.CompositingQuality = CompositingQuality.HighQuality;
        graphic.SmoothingMode = SmoothingMode.HighQuality;
        graphic.InterpolationMode = InterpolationMode.NearestNeighbor;
        graphic.DrawImage(baseImage, 0, 0, wt, ht);
        img.Save(Newname);

        img.Dispose();
        graphic.Dispose();
        baseImage.Dispose();

    }
}
public class FileUploadSetup
{
    private string _Name;
    public string name
    {
        get
        {
            return _Name;
        }
        set
        {
            _Name = value;
        }
    }
    private UpfileType _FileType;
    public UpfileType fileType
    {
        get
        {
            return _FileType;
        }
        set
        {
            _FileType = value;
        }
    }
    private string _SavePath;
    public string SavePath
    {
        get
        {
            return _SavePath;
        }
        set
        {
            _SavePath = value;
        }
    }
    private bool _AllowNoFile;
    public bool allowNoFile
    {
        get
        {
            return _AllowNoFile;
        }
        set
        {
            _AllowNoFile = value;
        }
    }
    private ArrayList _ThumbnailImages;
    public ArrayList ThumbnailImages
    {
        get
        {
            return _ThumbnailImages;
        }
        set
        {
            _ThumbnailImages = value;
        }
    }

    public FileUploadSetup()
    {
        name = "";
        fileType = UpfileType.File;
        SavePath = "";
        allowNoFile = true;
        ThumbnailImages = new ArrayList();
    }

    public enum UpfileType { File, Image } ;
}

public class ThumbnailImage
{
    private string _Suffix;
    public string suffix
    {
        get
        {
            return _Suffix;
        }
        set
        {
            _Suffix = value;
        }
    }
    private int _MaxWidth;
    public int maxWidth
    {
        get
        {
            return _MaxWidth;
        }
        set
        {
            _MaxWidth = value;
        }
    }
    private int _MaxHight;
    public int MaxHight
    {
        get
        {
            return _MaxHight;
        }
        set
        {
            _MaxHight = value;
        }
    }
}


