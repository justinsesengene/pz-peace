﻿<%@ Page Title="" Language="C#" MasterPageFile="~/manage/MasterPage.master" AutoEventWireup="true" CodeFile="Ready.aspx.cs" Inherits="manage_A01Permission_Ready" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div style="height: 400px;">
        <div>
            <br />
            <br />
            歡迎使用管理系統
            <br />
            <br />
            請您使用IE7以上瀏覽器，或是火狐與Google 瀏覽器，<br />
            解析度設定1024 * 768以上，以確保系統正常運作。</div>
            <hr />
    <asp:Label ID="Label5" runat="server" Text="尚未回覆資料:" ForeColor="Red" Font-Bold="True"></asp:Label>
            <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
        AutoGenerateColumns="False" DataKeyNames="id"  CssClass="tableStyle1"
        Width="80%" GridLines="None" 
        EnableModelValidation="True" onpageindexchanging="GridView1_PageIndexChanging" 
        PageSize="15">
        <Columns>
            <asp:BoundField DataField="ProductClassName" HeaderText="產品類別" 
                SortExpression="ProductClassName" />
            <asp:BoundField DataField="UnitClassName" HeaderText="機構類別" 
                SortExpression="UnitClassName" />
            <asp:BoundField DataField="AreaName" HeaderText="地區" 
                SortExpression="AreaName" />
            <asp:BoundField DataField="subject" HeaderText="主旨" SortExpression="subject" />
            <asp:BoundField DataField="poster" HeaderText="發佈人" SortExpression="poster" />
            <asp:BoundField DataField="initDate" DataFormatString="{0:d}" HeaderText="建立日期" 
                SortExpression="initDate" />
                <asp:TemplateField HeaderText="未處理數/總報名數">
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" ForeColor="Red" 
                            Text='<%# Bind("NoProcessregisterCount") %>'></asp:Label>
                        /<asp:Label ID="Label1" runat="server" Text='<%# Bind("registerCount") %>'></asp:Label>
                    </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="未回覆數/總詢問數">
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" ForeColor="Red" 
                        Text='<%# Eval("NoreplyfaqCount") %>'></asp:Label>
                    /<asp:Label ID="Label2" runat="server" Text='<%# Bind("faqCount") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("faqCount") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
                <asp:TemplateField HeaderText="編輯">
            <ItemTemplate><asp:HyperLink ID="lnkEdit"  runat="server" CssClass="action" 
                    title="編輯" ImageUrl="~/manage/images/Modify.gif" 
                    NavigateUrl='<%# "../C01Products/_Products_FAQ.aspx?id=" + Eval("id") %>' ></asp:HyperLink></ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    </div>
</asp:Content>

