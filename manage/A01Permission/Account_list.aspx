﻿<%@ Page Title="" Language="C#" MasterPageFile="~/manage/MasterPage.master" AutoEventWireup="true" CodeFile="Account_list.aspx.cs" Inherits="manage_A01Permission_Account_list" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div>
      &nbsp;<asp:Button ID="btnAdd" runat="server" Text="新增" 
          onclick="btnAdd_Click" />
          </div>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
        AutoGenerateColumns="False" DataKeyNames="id"  CssClass="tableStyle1"
        onrowdeleting="GridView1_RowDeleting" Width="80%" GridLines="None" 
        EnableModelValidation="True" onpageindexchanging="GridView1_PageIndexChanging" 
        PageSize="15">
        <Columns>
            <asp:BoundField DataField="account" HeaderText="帳號" 
                SortExpression="account" />
            <asp:BoundField DataField="Cname" HeaderText="名稱" SortExpression="Cname" />
            <asp:BoundField DataField="RoleName" HeaderText="類別" 
                SortExpression="RoleName" />
            <asp:BoundField DataField="initDate" DataFormatString="{0:d}" 
                HeaderText="建立日期" />
            <asp:TemplateField HeaderText="編輯">
            <ItemTemplate><asp:HyperLink ID="lnkEdit"  runat="server" CssClass="action" 
                    title="編輯" ImageUrl="~/manage/images/Modify.gif" 
                    NavigateUrl='<%# "Account_Edit.aspx?id=" + Eval("id") %>' ></asp:HyperLink></ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="刪除">
            <ItemTemplate>
            <asp:ImageButton ID="ibtnDelete" runat="server" CssClass="action"  CommandName="Delete" ImageUrl="~/manage/images/Delete.gif" OnClientClick="javascript:if(!window.confirm('你確定刪除嗎?')) return false;" />
            </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:AccessDataSource ID="AccessDataSource1" runat="server" 
        DataFile="~/App_Data/Wang.mdb" 
        SelectCommand="SELECT Account.id, Account.Account, Account.Cname, Account.initDate, Role.RoleName FROM (Account INNER JOIN Role ON Account.RoleID = Role.id)">
    </asp:AccessDataSource>
</asp:Content>
