﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class manage_A01Permission_Account_Edit : System.Web.UI.Page
{
   
    #region "將目前功能資料送到Master Page裡"
    protected void Page_Init(object sender, EventArgs e)
    {

        HiddenField FunctionNumber = (HiddenField)Master.FindControl("FunctionNumber");
        FunctionNumber.Value = "A01";
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            showRole();
            show();

        }
    }

    private void show()
    {
        EasyDataProvide Account = new EasyDataProvide("Account");
        DataRow row = Account.fillPageControlsByID(Request["id"], Page, "ContentPlaceHolder1");
        //Response.Write(Request["id"] + "__" + row["permission"].ToString());
        PermissionUserControl1.permissionString = row["permission"].ToString();
    }


    private void showRole()
    {
        EasyDataProvide Role = new EasyDataProvide("Role");
        DataTable dataTable = Role.getAllData();
        RoleID.DataTextField = "RoleName";
        RoleID.DataValueField = "id";
        RoleID.DataSource = dataTable;
        RoleID.DataBind();

    }
    protected void InsertButton_Click(object sender, EventArgs e)
    {
        EasyDataProvide Account = new EasyDataProvide("Account");
        Account.setPageFormQuest(Page, "ContentPlaceHolder1");
        string premissionString = PermissionUserControl1.permissionString;
        Account.addParameter("Permission", premissionString);
        Account.removeParameter("account");
        Account.UpdateById(Request["id"]);

        //取得寫入LOG
        EasyDataProvide Log = new EasyDataProvide("Log");
        string[] strUserData = ((FormsIdentity)(Page.User.Identity)).Ticket.UserData.Split(new Char[] { ';' });
        Log.addParameter("poster", strUserData[2]);
        Log.addParameter("subject", "編輯一筆資料");
        Log.addParameter("version", "C");
        Log.addParameter("moduleID", "A01");
        Log.Insert();
        My.WebForm.doJavaScript("alert('修改完畢!');location='Account_list.aspx';", Page);
    }
}