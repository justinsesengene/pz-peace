﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class manage_A01Permission_Ready : System.Web.UI.Page
{
    protected void Page_Init(object sender, EventArgs e)
    {
        HiddenField FunctionNumber = (HiddenField)Master.FindControl("FunctionNumber");
        FunctionNumber.Value = "Home";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            show();
        }
    }

    private void show()
    {
        //取得UserData
        string[] strUserData = ((FormsIdentity)(Page.User.Identity)).Ticket.UserData.Split(new Char[] { ';' });
        string UserName = "";
        if (strUserData[0] == "版主")
        {
            UserName = User.Identity.Name;
            EasyDataProvide View_Products_Count = new EasyDataProvide("View_Products_Count");
            View_Products_Count.addParameter("poster", UserName);
            DataTable dt = View_Products_Count.getData("poster=? and NoreplyfaqCount>0");
            GridView1.DataSource = dt;
            GridView1.DataBind();
            if (dt.Rows.Count == 0)
            {
                Label5.Visible = false;
            }
        }
        else
        {
            Label5.Visible = false;
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        show();
    }
}