﻿<%@ Page Title="" Language="C#" MasterPageFile="~/manage/MasterPage.master" AutoEventWireup="true" CodeFile="Account_Insert.aspx.cs" Inherits="manage_A01Permission_Account_Insert" %>
<%@ Register TagPrefix="uc1" TagName="PermissionUserControl" Src="~/UserControl/PermissionUserControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/jquery-1.5.2.min.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/jquery.validate.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/jquery.metadata.js") %>"></script>
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/jquery_validate.css") %>"
        rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () {
            $("form").validate({ meta: "validate" });
        });
           
            
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<fieldset>
        <legend>新增</legend>
        <div style="display: inherit; text-align: left;">
            <asp:Label ID="lblaccount" runat="server" AssociatedControlID="account">帳號</asp:Label><asp:TextBox
                ID="account" runat="server" Width="250px" CssClass="{validate:{required:true, messages:{required:'必填'}}}"></asp:TextBox></div>
        <div style="display: inherit; text-align: left;">
            <asp:Label ID="lblpassword" runat="server" AssociatedControlID="password">密碼</asp:Label><asp:TextBox
                ID="password" runat="server" Width="250px" CssClass="{validate:{required:true, messages:{required:'必填'}}}" TextMode="Password"></asp:TextBox></div>
        <div style="display: inherit; text-align: left;">
            <asp:Label ID="lblname" runat="server" AssociatedControlID="Cname">姓名</asp:Label><asp:TextBox
                ID="Cname" runat="server" Width="250px" CssClass="{validate:{required:true, messages:{required:'必填'}}}"></asp:TextBox></div>
        <div style="display: none; text-align: left;">
            <asp:Label ID="lblgroupBy" runat="server" AssociatedControlID="RoleID">類別</asp:Label><asp:DropDownList
                ID="RoleID" runat="server">
               
            </asp:DropDownList>
        </div>
    <div>
         <uc1:PermissionUserControl ID="PermissionUserControl1" runat="server" permissionString="" />
    </div>
        
        <div>
            <asp:Button ID="InsertButton" runat="server" Text="新增" OnClick="InsertButton_Click">
            </asp:Button><input type="button" value="回上頁" onclick="location='Account_list.aspx'" /></div>
        <div>
            <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label></div>
    </fieldset>
</asp:Content>

