﻿<%@ Page Title="" Language="C#" MasterPageFile="~/manage/MasterPage.master" AutoEventWireup="true" CodeFile="resources_List.aspx.cs" Inherits="manage_client_resources_List" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div>
      
       <div style="clear:both"></div>
      </div>

    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        CssClass="tableStyle1" DataKeyNames="id" 
        EnableModelValidation="True" GridLines="None" 
        onrowdeleting="GridView1_RowDeleting" 
        onrowupdating="GridView1_RowUpdating" AllowPaging="True" 
        onpageindexchanging="GridView1_PageIndexChanging">
        <Columns>
            <asp:BoundField DataField="company" HeaderText="公司名稱" 
                SortExpression="company" />

            <asp:TemplateField HeaderText="聯 絡 人" SortExpression="name" >
                    <ItemTemplate>
                        <asp:HyperLink ID="hyName" runat="server" Text='<%# Eval("name") %>' NavigateUrl='<%# "resources_Detail.aspx?ModuleID=" +Request["ModuleID"]+"&ID="+Eval("id") %>'></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            <asp:TemplateField HeaderText="E m a i l" SortExpression="emailAddress" >
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("emailAddress") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            <asp:BoundField DataField="initDate" DataFormatString="{0:d}" HeaderText="發表日期" 
                SortExpression="initDate" />
                <asp:TemplateField HeaderText="編輯" Visible="False">
                    <ItemTemplate>
                        <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="編輯" 
                            CommandName="Update" ImageUrl="../images/Modify.gif" />
                    </ItemTemplate>
                   
                </asp:TemplateField>
                <asp:TemplateField HeaderText="刪除" ShowHeader="False" Visible="False">
                    <ItemTemplate>
                        <asp:ImageButton ID="ibtnDelete" runat="server" CssClass="action"  CommandName="Delete" ImageUrl="../images/Delete.gif" OnClientClick="javascript:if(!window.confirm('你確定刪除嗎?')) return false;" />
                    </ItemTemplate>
                    
             </asp:TemplateField>
        </Columns>

    </asp:GridView>
    </asp:Content>

