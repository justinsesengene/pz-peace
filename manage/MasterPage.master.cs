﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Web.Security;
using System.Xml.Xsl;

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Init(object sender, EventArgs e)
    {
        //驗證身份
        if (Page.User.Identity.IsAuthenticated == false)
        {

        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack == false)
        {
            //取得UserData
            string[] strUserData = ((FormsIdentity)(Page.User.Identity)).Ticket.UserData.Split(new Char[] { ';' });
            //  是否有權限
            if (FunctionNumber.Value == "Home")
            {
                content_title.Visible = false;
            }
            if (strUserData.Length != 3)
            {
                Response.Redirect("~/manage/Login.aspx");
            }


            lblUserName.Text = strUserData[2];
            getMenu(strUserData[1]);
            //Response.Write(strUserData[1]);
        }

    }

    protected void LoginStatus1_LoggingOut(object sender, LoginCancelEventArgs e)
    {
        FormsAuthentication.SignOut();
        Response.Redirect("~/manage/Login.aspx");
    }

    void getMenu(string permission)
    {
        //XmlDocument xmldoc = new XmlDocument();
        //xmldoc.Load(Server.MapPath("~/manage/Menu.xml"));
        //System.Text.StringBuilder sb = new System.Text.StringBuilder("<ul id=\"flyout\">");
        //recursionXml(xmldoc.DocumentElement, sb, permission);
        //sb.Append("</ul>");
        //litMenu.Text = sb.ToString();

        StringBuilder stringBuilder = new StringBuilder();
        StringWriter sw = new StringWriter(stringBuilder);
        XmlDocument doc = new XmlDocument();
        doc.Load(Server.MapPath("~/manage/Menu.xml"));
        XmlNode xmlNode = doc.SelectSingleNode("//Modules[@value='" + FunctionNumber.Value + "']");
        if (xmlNode != null) litFunctionName.Text = xmlNode.Attributes["Title"].Value;
        XsltArgumentList args = new XsltArgumentList();
        args.AddParam("permission", "", permission);
        args.AddParam("moduleID", "", FunctionNumber.Value);
        XslCompiledTransform xslt = new XslCompiledTransform();
        xslt.Load(Server.MapPath("~/manage/menu.xslt"));
        xslt.Transform(doc, args, sw);
        litMenu.Text = stringBuilder.ToString().Replace("~/manage/", Page.ResolveUrl("~/manage/"));

    }

    void recursionPublish(DataRowView row, DataTable dtPublish, System.Text.StringBuilder sb, string permission)
    {
        if (permission.IndexOf(row["functionName"].ToString()) != -1)
        {
            DataView dv = new DataView(dtPublish);
            dv.RowFilter = string.Format("parentID='{0}'", row["functionName"]);
            dv.Sort = "listNum";
            if (dv.Count > 0) //目錄
            {
                string classstyle = "class=\"fly\"";
                if (FunctionNumber.Value.IndexOf(row["functionName"].ToString()) != -1)
                {
                    classstyle = "class=\"fly current\"";
                }
                sb.Append(String.Format("<li><a {0} href=\"#\"><b>{1}</b><!--[if gte IE 7]><!--></a><!--<![endif]--><!--[if lte IE 6]><table><tr><td><![endif]--><ul>", classstyle, row["className"]));
                foreach (DataRowView childRow in dv)
                {
                    recursionPublish(childRow, dtPublish, sb, permission);
                }

                sb.Append("</ul><!--[if lte IE 6]></td></tr></table></a><![endif]--></li>");
            }
            else
            {
                string classstyle = "";
                if (row["functionName"].ToString() == FunctionNumber.Value)
                {
                    litFunctionName.Text = row["className"].ToString();
                    classstyle = "class=\"current\"";
                }
                sb.Append(String.Format("<li><a {0} href=\"{1}\"><b>{2}</b></a></li>", classstyle, Page.ResolveUrl(string.Format("~/Publish/list/{0}", row["functionName"])), row["className"]));
            }
        }


    }

    void recursionXml(XmlNode pnode, System.Text.StringBuilder sb, string permission)
    {
        foreach (XmlNode node in pnode.ChildNodes)
        {

            if (permission.IndexOf(node.Attributes["value"].Value) != -1)
            {
                if (node.HasChildNodes) //目錄
                {
                    string classstyle = "class=\"fly\"";
                    if (FunctionNumber.Value.IndexOf(node.Attributes["value"].Value) != -1)
                    {
                        classstyle = "class=\"fly current\"";
                    }
                    sb.Append(String.Format("<li><a {0} href=\"#\"><b>{1}</b><!--[if gte IE 7]><!--></a><!--<![endif]--><!--[if lte IE 6]><table><tr><td><![endif]--><ul>", classstyle, node.Attributes["Title"].Value));
                    recursionXml(node, sb, permission);
                    sb.Append("</ul><!--[if lte IE 6]></td></tr></table></a><![endif]--></li>");
                }
                else
                { //子結點
                    string classstyle = "";
                    if (node.Attributes["value"].Value == FunctionNumber.Value)
                    {
                        litFunctionName.Text = node.Attributes["Title"].Value;
                        classstyle = "class=\"current\"";
                    }
                    sb.Append(String.Format("<li><a {0} href=\"{1}\"><b>{2}</b></a></li>", classstyle, Page.ResolveUrl(node.Attributes["url"].Value), node.Attributes["Title"].Value));
                }
            }

        }
    }

}
