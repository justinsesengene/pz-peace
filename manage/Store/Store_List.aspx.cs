﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class manage_Store_Store_List : System.Web.UI.Page
{
    #region "將目前功能資料送到Master Page裡"
    protected void Page_Init(object sender, EventArgs e)
    {

        HiddenField FunctionNumber = (HiddenField)Master.FindControl("FunctionNumber");
        FunctionNumber.Value = "B01";
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            showClass();
            show();
        }
    }
    private void showClass()
    {
        EasyDataProvide StoreClass = new EasyDataProvide("StoreClass");
        StoreClass.addParameter("moduleID", "B01");
        DataTable dtProductClass = StoreClass.getData(String.Format("version='C' and moduleID=?"), "order by listNum asc");
        ddlClass.DataTextField = "className";
        ddlClass.DataValueField = "id";
        ddlClass.DataSource = dtProductClass;
        ddlClass.DataBind();
        ListItem item = new ListItem("全部", "-1");
        ddlClass.Items.Insert(0, item);
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        ddlClass.SelectedValue = "-1";
        HiddenField1.Value = "";
        show();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        EasyDataProvide Store = new EasyDataProvide("Store");
        Store.addParameter("moduleID", "B01");
        StringBuilder searchStringBuilder = new StringBuilder();
        if (ddlClass.SelectedValue != "-1")
        {
            Store.addParameter("classID", ddlClass.SelectedValue);
            searchStringBuilder.Append(" and classID=?");
        }
        if (!string.IsNullOrEmpty(HiddenField1.Value))
        {
            Store.addParameter("zip", HiddenField1.Value);
            searchStringBuilder.Append(" and zip=?");
        }

        DataTable dataTable = Store.getData(String.Format("version='C' and moduleID=? " + searchStringBuilder), "order by listNum asc");
        //DataTable dataTable = Store.getData(String.Format("version='C' and moduleID=?"), "order by listNum asc");
        GridView1.DataSource = dataTable;
        GridView1.DataBind();
    }

    protected void ddlClass_SelectedIndexChanged(object sender, EventArgs e)
    {
        show();
    }
    private void show()
    {
        EasyDataProvide Store = new EasyDataProvide("Store");
        Store.addParameter("moduleID", "B01");
        Store.addParameter("classID", ddlClass.SelectedValue);
        //DataTable dataTable = Store.getData(String.Format("version='C' and moduleID=? and classID=?"), "order by listNum asc");
        DataTable dataTable = Store.getData(String.Format("version='C' and moduleID=?"), "order by listNum asc");
        GridView1.DataSource = dataTable;
        GridView1.DataBind();
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string strID = GridView1.DataKeys[e.RowIndex].Value.ToString();
        EasyDataProvide Store = new EasyDataProvide("Store");
        Store.DeleteById(strID);

        //取得寫入LOG
        //EasyDataProvide Log = new EasyDataProvide("Log");
        //string[] strUserData = ((FormsIdentity)(Page.User.Identity)).Ticket.UserData.Split(new Char[] { ';' });
        //Log.addParameter("poster", strUserData[2]);
        //Log.addParameter("subject", "刪除一筆資料");
        //Log.addParameter("version", "C");
        //Log.addParameter("moduleID", "B01");
        //Log.Insert();

        ddlClass.SelectedValue = "-1";
        HiddenField1.Value = "";
        show();
    }
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string strID = GridView1.DataKeys[e.RowIndex].Value.ToString();
        string page = Request["page"] ?? "1";
        Response.Redirect("Store_Edit.aspx?ID=" + strID + "&ModuleID=" + Request["ModuleID"] + "&page=" + page);
    }
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        string strID = GridView1.DataKeys[e.NewEditIndex].Value.ToString();
        string page = Request["page"] ?? "1";
        Response.Redirect("Store_Upload.aspx?ID=" + strID + "&ModuleID=" + Request["ModuleID"] + "&page=" + page);
    }
    
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        show();
    }
}