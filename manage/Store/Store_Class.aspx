﻿<%@ Page Title="" Language="C#" MasterPageFile="~/manage/MasterPage.master" AutoEventWireup="true" CodeFile="Store_Class.aspx.cs" Inherits="manage_Store_Store_Class" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div>
    新增類別:<asp:TextBox ID="txtAdd" runat="server"></asp:TextBox>
    <asp:Button ID="btnAdd" runat="server"
        Text="確定" onclick="btnAdd_Click" />
        <input onclick="location = 'Store_List.aspx'" type="button" value="回列表" />
   </div>    
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        DataKeyNames="id" 
        EnableModelValidation="True" CssClass="tableStyle1" GridLines="None" 
        onrowdatabound="GridView1_RowDataBound" 
        onrowcancelingedit="GridView1_RowCancelingEdit" 
        onrowdeleting="GridView1_RowDeleting" onrowediting="GridView1_RowEditing" 
        onrowupdating="GridView1_RowUpdating">
        <Columns>
            <asp:TemplateField HeaderText="名稱" SortExpression="className">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("className") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("className") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="原始排序" SortExpression="className">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("listNum") %>'></asp:Label>
                    </ItemTemplate>
                   
                </asp:TemplateField>
                <asp:TemplateField HeaderText="新排序">
                    <EditItemTemplate>
                    <asp:TextBox ID="listNum" runat="server" CssClass="position {validate:{required:true,number:true, messages:{required:'新排序必填',number:'須為數字'}}}" Width="40px" 
                            Text='<%# Eval("listNum") %>'></asp:TextBox>
                </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("listNum") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            <asp:CommandField HeaderText="編輯" ShowEditButton="True" />
            <asp:CommandField HeaderText="刪除" ShowDeleteButton="True" />
        </Columns>
    </asp:GridView>
    <div style="display: none">
 <asp:Button ID="btnSure" runat="server" Text="更新排序" CssClass="button" 
                onclick="btnSure_Click" /> (請拖曳更改排序)
                </div>
</asp:Content>

