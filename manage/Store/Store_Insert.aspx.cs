﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class manage_Store_Store_Insert : System.Web.UI.Page
{
    #region "將目前功能資料送到Master Page裡"
    protected void Page_Init(object sender, EventArgs e)
    {

        HiddenField FunctionNumber = (HiddenField)Master.FindControl("FunctionNumber");
        FunctionNumber.Value = "B01";
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            EasyDataProvide StoreClass = new EasyDataProvide("StoreClass");
            StoreClass.addParameter("moduleID", "B01");
            DataTable dtProductClass = StoreClass.getData(String.Format("version='C' and moduleID=?"), "order by listNum asc");
            ddlClass.DataTextField = "className";
            ddlClass.DataValueField = "id";
            ddlClass.DataSource = dtProductClass;
            ddlClass.DataBind();
        }
    }
    protected void InsertButton_Click(object sender, EventArgs e)
    {
        //if (string.IsNullOrEmpty(content.Text))
        //{
        //    lblError.Text = "請輸入資料";
        //    return;
        //}
        EasyDataProvide Store = new EasyDataProvide("Store");
        Store.addParameter("id", Guid.NewGuid().ToString());
        Store.setPageFormQuest(Page, "ContentPlaceHolder1");
        //處理上傳檔案
        if (ddlFile.SelectedValue == "檔案上傳" && fuFile.HasFile)
        {
            //取得副檔名
            string Extension = fuFile.FileName.Split('.')[fuFile.FileName.Split('.').Length - 1];
            //新檔案名稱
            string fileName = String.Format("{0:yyyyMMddhhmmsss}.{1}", DateTime.Now, Extension);
            fuFile.SaveAs(Server.MapPath(String.Format("~/UploadFiles/Files/{0}", fileName)));
            Store.addParameter("fileUrl", fileName);

        }
        System.Threading.Thread.Sleep(1000);
        //處理上傳PDF檔案
        if (ddlPDF.SelectedValue == "檔案上傳" && fuPDF.HasFile)
        {
            //取得副檔名
            string Extension = fuPDF.FileName.Split('.')[fuPDF.FileName.Split('.').Length - 1];
            if (Extension.ToLower() != "pdf")
            {
                My.WebForm.doJavaScript("alert('pdf檔錯誤!');", Page);
                return;
            }
            //新檔案名稱
            string fileName = String.Format("{0:yyyyMMddhhmmsss}.{1}", DateTime.Now, Extension);
            fuPDF.SaveAs(Server.MapPath(String.Format("~/UploadFiles/Files/{0}", fileName)));
            Store.addParameter("pdfUrl", fileName);

        }
        //處理上傳圖片

        if (fuPic.HasFile)
        {
            if (fuPic.PostedFile.ContentType.IndexOf("image") == -1)
            {
                My.WebForm.doJavaScript("alert('檔案型態錯誤!');", Page);
                return;
            }

            //取得副檔名
            string Extension = fuPic.FileName.Split('.')[fuPic.FileName.Split('.').Length - 1];
            //新檔案名稱
            string fileName = String.Format("{0:yyyyMMddhhmmsss}.{1}", DateTime.Now, Extension);
            fuPic.SaveAs(Server.MapPath(String.Format("~/UploadFiles/Images/{0}", fileName)));

            Store.addParameter("picUrl", fileName);
            //產生縮圖

            My.WebForm.GenerateThumbnailImage(fileName, fuPic.PostedFile.InputStream, Server.MapPath("~/UploadFiles/Images/"), "s", 621, 465);
        }
        Store.addParameter("classID", ddlClass.SelectedValue);
        Store.addParameter("version", "C");
        Store.addParameter("city", Request["city"]);
        Store.addParameter("division", Request["division"]);
        Store.addParameter("zip", Request["zip"]);
        Store.addParameter("moduleID", "B01");

        //不填結束日期時，設定一個800年後的日期
        if (string.IsNullOrEmpty(endDate.Text))
        {
            Store.addParameter("endDate", "2800/1/1");
        }
        Store.Insert();
        //取得寫入LOG
        //EasyDataProvide Log = new EasyDataProvide("Log");
        //string[] strUserData = ((FormsIdentity)(Page.User.Identity)).Ticket.UserData.Split(new Char[] { ';' });
        //Log.addParameter("poster", strUserData[2]);
        //Log.addParameter("subject", "新增一筆資料");
        //Log.addParameter("version", "C");
        //Log.addParameter("moduleID", "B01");
        //Log.Insert();
        Response.Redirect("Store_List.aspx?ModuleID=" + "B01");
    }
    protected void ddlFile_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlFile.SelectedValue == "檔案上傳")
        {
            fuFile.Visible = true;
            fileUrl.Visible = false;
        }
        else
        {
            fuFile.Visible = false;
            fileUrl.Visible = true;
        }


    }
    protected void ddlPDF_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlPDF.SelectedValue == "檔案上傳")
        {
            fuPDF.Visible = true;
            pdfUrl.Visible = false;
        }
        else
        {
            fuPDF.Visible = false;
            pdfUrl.Visible = true;
        }
    }
}