﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class manage_Store_Store_Upload : System.Web.UI.Page
{
    #region "將目前功能資料送到Master Page裡"
    protected void Page_Init(object sender, EventArgs e)
    {

        HiddenField FunctionNumber = (HiddenField)Master.FindControl("FunctionNumber");
        FunctionNumber.Value = "B01";
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            show();
        }
    }
    private void show()
    {
        EasyDataProvide Store = new EasyDataProvide("Store");
        DataRow rowStore = Store.getByID(Request["ID"]);
        lblSubject.Text = rowStore["subject"].ToString();
        //圖片列表
        EasyDataProvide ModulePictures = new EasyDataProvide("ModulePictures");
        ModulePictures.addParameter("publishID", Request["ID"]);
        DataTable dtPic = ModulePictures.getData("publishID=@publishID", "order by listNum ASC");
        if (dtPic.Rows.Count > 0)
        {
            GridView1.DataSource = dtPic;
            GridView1.DataBind();
        }
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[4].Attributes.Add("onclick",
                                          "javascript:if(!window.confirm('你確定刪除嗎?')) return false;");
            string picID = GridView1.DataKeys[e.Row.RowIndex].Value.ToString();
            HyperLink lnkEdit = (HyperLink)e.Row.FindControl("lnkEdit");
            lnkEdit.NavigateUrl =
                String.Format(
                    "Store_Picture_Edit.aspx?ModuleID={0}&picID={1}&ID={2}&KeepThis=true&TB_iframe=true&height=200&width=600",
                    Request["ModuleID"], picID,Request["ID"]);
        }
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string strID = GridView1.DataKeys[e.RowIndex].Value.ToString();
        EasyDataProvide ModulePictures = new EasyDataProvide("ModulePictures");
        ModulePictures.DeleteById(strID);

        //取得寫入LOG
        //EasyDataProvide Log = new EasyDataProvide("Log");
        //string[] strUserData = ((FormsIdentity)(Page.User.Identity)).Ticket.UserData.Split(new Char[] { ';' });
        //Log.addParameter("poster", strUserData[2]);
        //Log.addParameter("subject", "刪除一張照片");
        //Log.addParameter("version", "C");
        //Log.addParameter("moduleID", "B01");
        //Log.Insert();
        //Response.Redirect("authenticate_Edit.aspx?ModuleID=" + Request["ModuleID"]);

        show();
    }
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string strID = GridView1.DataKeys[e.RowIndex].Value.ToString();
        string page = Request["page"] ?? "1";
        Response.Redirect("Store_Upload.aspx?ID=" + strID + "&ModuleID=" + Request["ModuleID"] + "&page=" + page);
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        show();
    }
}