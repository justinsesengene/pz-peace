﻿<%@ Page Title="" Language="C#" MasterPageFile="~/manage/MasterPage.master" AutoEventWireup="true" CodeFile="Store_Upload.aspx.cs" Inherits="manage_Store_Store_Upload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/jquery-1.4.2.min.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/jquery.validate.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/jquery.metadata.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/ui.datepicker.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/ui.datepicker-zh-TW.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/jquery.maxlength-min.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/Scripts/ckeditor/ckeditor.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/thickbox.js") %>"></script>
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/jquery_validate.css") %>"
        rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/ui.core.css") %>"
        rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/ui.datepicker.css") %>"
        rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/ui.theme.css") %>"
        rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/single_seventeen.css") %>"
        rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/maxlength.css") %>"
        rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/thickbox.css") %>" rel="stylesheet"
        type="text/css" />
     <script type="text/javascript">
         $(document).ready(function () {
             $("form").validate({ meta: "validate" });
             $(".datepicker input").datepicker();
         });


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>
        <asp:Label ID="lblSubject" runat="server"></asp:Label></h1>
     <fieldset>
    <legend>圖片列表</legend>
         <div>
    <a href="<%=String.Format("Store_Picture_Insert.aspx?ModuleID={0}&ID={1}&KeepThis=true&TB_iframe=true&height=200&width=600", Request["ModuleID"],Request["ID"]) %>" title="上傳圖片" class="thickbox" >上傳圖片</a>
    </div>
          <div style="display: inherit">
              <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        CssClass="tableStyle1" DataKeyNames="id" 
        EnableModelValidation="True" GridLines="None" 
        onrowdeleting="GridView1_RowDeleting" 
        onrowupdating="GridView1_RowUpdating" AllowPaging="True" 
        onpageindexchanging="GridView1_PageIndexChanging" onrowdatabound="GridView1_RowDataBound">
        <Columns>
            <asp:BoundField DataField="picName" HeaderText="圖片標題" 
                SortExpression="picName" />
            <asp:BoundField DataField="listNum" HeaderText="圖片排序" 
                SortExpression="listNum" />
            <asp:TemplateField HeaderText="圖片檢視" SortExpression="version">
                    <ItemTemplate>
                        <asp:HyperLink ID="fuPicLink" runat="server" Target="_blank" NavigateUrl='<%# "~/UploadFiles/Images/"+Eval("picUrl") %>'>檢視圖片</asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>

                   <asp:TemplateField HeaderText="編輯">
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkEdit" runat="server" CssClass="thickbox" >編輯</asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            <asp:CommandField ShowDeleteButton="True" HeaderText="刪除" />

        </Columns>

    </asp:GridView>
              </div>
         <div><input onclick="location = 'Store_List.aspx'" type="button" value="回列表" /></div>
</fieldset>
</asp:Content>

