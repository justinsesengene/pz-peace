﻿<%@ Page Title="" Language="C#" MasterPageFile="~/manage/MasterPage.master" AutoEventWireup="true" CodeFile="Store_List.aspx.cs" Inherits="manage_Store_Store_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/jquery-1.6.3.min.js") %>"></script>
     <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/twzipcode-1.3.1.js") %>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
           
            $('.zipcode').twzipcode({
                zipSel: $('#hidzip').children().val(),
                zipReadonly: false
            });
            $(".zipcode").change(function () {
                $('#hidzip').children().val($(".zipcode").find("input").val());
            });
        });


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div>
      <input onclick="location = 'Store_Insert.aspx'" type="button" value="新增商店" class="Addbutton" /> 
    <input onclick="location = 'Store_Class.aspx'" type="button" value="商家類別管理" class="Addbutton" />
       <div style="clear:both"></div>
      </div>
    <div style="display: inherit">
             <asp:Label ID="Label4" runat="server" AssociatedControlID="ddlClass">搜尋條件</asp:Label>
                <asp:DropDownList ID="ddlClass" runat="server"></asp:DropDownList>
                <span class="zipcode"></span>

                <span id="hidzip">
                    <asp:HiddenField ID="HiddenField1" runat="server" />
                </span>
        <asp:Button ID="btnSearch" runat="server" Text="搜尋" OnClick="btnSearch_Click"/>
        <asp:Button ID="btnReset" runat="server" Text="清除" OnClick="btnReset_Click"/>
        </div>
    <div style="clear: both"></div>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        CssClass="tableStyle1" DataKeyNames="id" 
        EnableModelValidation="True" GridLines="None" 
        onrowdeleting="GridView1_RowDeleting" 
        OnRowEditing="GridView1_RowEditing" 
        onrowupdating="GridView1_RowUpdating" AllowPaging="True" 
        onpageindexchanging="GridView1_PageIndexChanging">
        <Columns>
            <asp:TemplateField HeaderText="商店名稱" SortExpression="subject">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("subject") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="縣市" SortExpression="city">
                <ItemTemplate>
                    <asp:Label ID="lblcity" runat="server" Text='<%# Eval("city") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="區域" SortExpression="division">
                <ItemTemplate>
                    <asp:Label ID="lbldivision" runat="server" Text='<%# Eval("division") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="郵遞區號" SortExpression="zip">
                <ItemTemplate>
                    <asp:Label ID="lblzip" runat="server" Text='<%# Eval("zip") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="listNum" HeaderText="排序" 
                SortExpression="listNum" Visible="False"/>
            <asp:BoundField DataField="initDate" DataFormatString="{0:d}" HeaderText="建立日期" 
                SortExpression="initDate" />
            <asp:TemplateField HeaderText="上傳圖片">
                    <ItemTemplate>
                        <asp:ImageButton ID="ibtnUpload" runat="server" AlternateText="上傳圖片" 
                            CommandName="Edit" ImageUrl="../images/arrow_up_blue.gif" />
                    </ItemTemplate>
                   
                </asp:TemplateField>
                <asp:TemplateField HeaderText="編輯">
                    <ItemTemplate>
                        <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="編輯" 
                            CommandName="Update" ImageUrl="../images/Modify.gif" />
                    </ItemTemplate>
                   
                </asp:TemplateField>
                <asp:TemplateField HeaderText="刪除" ShowHeader="False">
                    <ItemTemplate>
                        <asp:ImageButton ID="ibtnDelete" runat="server" CssClass="action"  CommandName="Delete" ImageUrl="../images/Delete.gif" OnClientClick="javascript:if(!window.confirm('你確定刪除嗎?')) return false;" />
                    </ItemTemplate>
                    
             </asp:TemplateField>
        </Columns>

    </asp:GridView>
</asp:Content>

