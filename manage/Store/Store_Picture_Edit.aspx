﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Store_Picture_Edit.aspx.cs" Inherits="manage_Store_Store_Picture_Edit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/jquery-1.4.2.min.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/jquery.validate.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/jquery.metadata.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/ui.datepicker.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/ui.datepicker-zh-TW.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/jquery.maxlength-min.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/Scripts/ckeditor/ckeditor.js") %>"></script>
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/jquery_validate.css") %>"
        rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/ui.core.css") %>"
        rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/ui.datepicker.css") %>"
        rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/ui.theme.css") %>"
        rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/single_seventeen.css") %>"
        rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/maxlength.css") %>"
        rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () {
            $("form").validate({ meta: "validate" });
            $(".datepicker input").datepicker();
        });


    </script>
</head>
<body>
    <form id="form1" runat="server">
   <div>
    <fieldset>
        <legend>圖片編輯</legend>
        
<div style="display:inherit;text-align: left;"><asp:label ID="lblpicName" runat="server" AssociatedControlID="picName">圖片標題</asp:label><asp:TextBox ID="picName" runat="server" width="250px" CssClass="{validate:{required:true, messages:{required:'圖片標題必填'}}}"></asp:TextBox></div>
        <div style="display:inherit;text-align: left;"><asp:label ID="Label1" runat="server" AssociatedControlID="listNum">圖片排序</asp:label><asp:TextBox ID="listNum" runat="server" width="50px" CssClass="{validate:{required:true,digits:true, messages:{required:'圖片排序必填',digits:'必須為整數'}}}"></asp:TextBox>
            
            <asp:HyperLink ID="fuPicLink" runat="server" Target="_blank" >檢視圖片</asp:HyperLink>
        </div>
<div style="display:inherit;text-align: left;"><asp:label ID="lblpicUrl" runat="server" AssociatedControlID="fuPic">選擇圖片</asp:label>
            <asp:FileUpload ID="fuPic" runat="server" />
</div>
       
         <div style="color:White;font-weight:bold;margin-top:20px;">
    <asp:Button ID="btnEdit" runat="server" Text="確定"  CssClass="button" onclick="btnEdit_Click"/>
             <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
</div> 
    </fieldset>
    </div>
    </form>
</body>
</html>
