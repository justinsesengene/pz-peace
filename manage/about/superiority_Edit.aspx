﻿<%@ Page ValidateRequest="false" Title="" Language="C#" MasterPageFile="~/manage/MasterPage.master" AutoEventWireup="true" CodeFile="superiority_Edit.aspx.cs" Inherits="manage_about_superiority_Edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/jquery-1.4.2.min.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/jquery.validate.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/jquery.metadata.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/ui.datepicker.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/ui.datepicker-zh-TW.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/jquery.maxlength-min.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/Scripts/ckeditor/ckeditor.js") %>"></script>
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/jquery_validate.css") %>"
        rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/ui.core.css") %>"
        rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/ui.datepicker.css") %>"
        rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/ui.theme.css") %>"
        rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/single_seventeen.css") %>"
        rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/maxlength.css") %>"
        rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () {
            $("form").validate({ meta: "validate" });
            $(".datepicker input").datepicker();
        });


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<fieldset>
<legend>編輯我們的優勢</legend>
    <div style="display: inherit">
            <div>
                <asp:Label ID="lblcontent" runat="server" AssociatedControlID="content">詳細內容</asp:Label>
            </div>
            <div style="clear:both">
                <asp:TextBox ID="content" runat="server" CssClass="ckeditor" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
         

<div><asp:Button ID="InsertButton" runat="server"  Text="確定" 
                 onclick="InsertButton_Click"></asp:Button><input type="button" value="回上一頁" onclick="history.back()"/></div><div>
<asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label></div>
</fieldset>
    <asp:Panel ID="Panel1" runat="server" Visible="False">
        <div style="display: inherit">
            <asp:Label ID="lblpicUrl" runat="server" AssociatedControlID="fuPic">相關圖片</asp:Label>
            <asp:FileUpload ID="fuPic" runat="server" />
            <asp:HyperLink ID="fuPicLink" runat="server" Target="_blank">檢視圖片</asp:HyperLink>
        </div>
        <div style="display: inherit">
            <asp:Label ID="lbllinkUrl" runat="server" AssociatedControlID="linkUrl">相關連結</asp:Label><asp:TextBox
                ID="linkUrl" runat="server" Width="250px"></asp:TextBox>
            <asp:DropDownList ID="linkTarget" runat="server">
                <asp:ListItem Value="_blank">另開新視窗</asp:ListItem>
                <asp:ListItem Value="_self">在當前視窗開啟</asp:ListItem>
            </asp:DropDownList>
            (外連結請加http://)
        </div>
        <div style="display: inherit">
            <asp:Label ID="lbllinkText" runat="server" AssociatedControlID="linkText">連結說明文字</asp:Label><asp:TextBox
                ID="linkText" runat="server" Width="250px"></asp:TextBox></div>
        <div style="display:inherit;text-align: left;"><asp:label ID="lblsubject" runat="server" AssociatedControlID="subject">名稱</asp:label><asp:TextBox ID="subject" runat="server" width="250px" CssClass="{validate:{required:true, messages:{required:'標題必填'}}}"></asp:TextBox></div>
<div style="display: inherit" class="datepicker">
            <asp:Label ID="lblstartDate" runat="server" AssociatedControlID="startDate" >開始日期</asp:Label><asp:TextBox
                ID="startDate" runat="server" Width="250px" CssClass="{validate:{required:true, messages:{required:'開始日期必填'}}}"></asp:TextBox></div>
        <div style="display: inherit" class="datepicker">
            <asp:Label ID="lblendDate" runat="server" AssociatedControlID="endDate">結束日期</asp:Label><asp:TextBox
                ID="endDate" runat="server" Width="250px" CssClass="{validate:{date:true, messages:{date:'結束日期格式不正確'}}}"></asp:TextBox>
            (不設定代表永遠生效)</div>
            <div style="display: inherit">
            <asp:Label ID="Label1" runat="server" AssociatedControlID="pdfUrl">上傳pdf檔</asp:Label><asp:DropDownList
                ID="ddlPDF" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="ddlPDF_SelectedIndexChanged">
                <asp:ListItem>檔案上傳</asp:ListItem>
                <asp:ListItem Selected="True">檔案連結</asp:ListItem>
            </asp:DropDownList>
            <asp:FileUpload ID="fuPDF" runat="server" Visible="False" />
            <asp:TextBox ID="pdfUrl" runat="server" Width="250px" ></asp:TextBox>
            <asp:HyperLink ID="pdfUrlLink" runat="server" Target="_blank">下載檔案</asp:HyperLink>
            </div>

            <div style="display: inherit">
            <asp:Label ID="lblfileUrl" runat="server" AssociatedControlID="fileUrl">檔案</asp:Label><asp:DropDownList
                ID="ddlFile" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlFile_SelectedIndexChanged">
                <asp:ListItem>檔案連結</asp:ListItem>
                <asp:ListItem>檔案上傳</asp:ListItem>
            </asp:DropDownList>
            <asp:FileUpload ID="fuFile" runat="server" Visible="False" />
            <asp:TextBox ID="fileUrl" runat="server" Width="250px"></asp:TextBox>
            <asp:HyperLink ID="fileUrlLink" runat="server" Target="_blank">下載檔案</asp:HyperLink>
        </div>

        
        </asp:Panel>
</asp:Content>
