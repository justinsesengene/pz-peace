﻿<%@ Page ValidateRequest="false" Title="" Language="C#" MasterPageFile="~/manage/MasterPage.master"
    AutoEventWireup="true" CodeFile="FAQ_Edit.aspx.cs" Inherits="manage_B02FAQ_FAQ_Edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/jquery-1.4.2.min.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/jquery.validate.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/jquery.metadata.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/ui.datepicker.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/ui.datepicker-zh-TW.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/jquery.maxlength-min.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/Scripts/ckeditor/ckeditor.js") %>"></script>
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/jquery_validate.css") %>"
        rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/ui.core.css") %>"
        rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/ui.datepicker.css") %>"
        rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/ui.theme.css") %>"
        rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/single_seventeen.css") %>"
        rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/maxlength.css") %>"
        rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () {
            $("form").validate({ meta: "validate" });
            $(".datepicker input").datepicker();
        });
           
            
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <fieldset>
        <legend>常見問題</legend>
        
<div style="display: none">
            <asp:Label ID="Label2" runat="server" AssociatedControlID="className">分類</asp:Label>
    <asp:DropDownList ID="className" runat="server">
        <asp:ListItem>分類一</asp:ListItem>
        <asp:ListItem>分類二</asp:ListItem>
        <asp:ListItem>分類三</asp:ListItem>
    </asp:DropDownList>
            </div>
         <div style="display: inherit">
             <asp:Label ID="Label4" runat="server" AssociatedControlID="ddlClass">類別</asp:Label>
             <asp:DropDownList ID="ddlClass" runat="server"></asp:DropDownList>
        </div>
        <div style="display: inherit">
             <asp:Label ID="lblListNum" runat="server" AssociatedControlID="listNum">排序</asp:Label>
             <asp:TextBox ID="listNum" runat="server" Width="50px" ></asp:TextBox>
        </div>
        <div style="display: inherit">
            <div>
                <asp:Label ID="lblcontent" runat="server" AssociatedControlID="content">問題</asp:Label>
            </div>
            <div style="clear: both">
                <asp:TextBox ID="content" runat="server"  CssClass="ckeditor" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
        <div style="display: inherit">
            <div>
                <asp:Label ID="Label3" runat="server" AssociatedControlID="article">回答</asp:Label>
            </div>
            <div style="clear: both">
                <asp:TextBox ID="article" runat="server"  CssClass="ckeditor"  TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
         <asp:Panel ID="Panel1" runat="server" Visible="false">
         <div style="display: inherit; text-align: left;">
            <asp:Label ID="lblsubject" runat="server" AssociatedControlID="subject">名稱</asp:Label><asp:TextBox
                ID="subject" runat="server" Width="250px" CssClass="{validate:{required:true, messages:{required:'標題必填'}}}"></asp:TextBox></div>

                 <div style="display: inherit" class="datepicker">
            <asp:Label ID="lblstartDate" runat="server" AssociatedControlID="startDate">開始日期</asp:Label><asp:TextBox
                ID="startDate" runat="server" Width="250px" CssClass="{validate:{required:true, messages:{required:'開始日期必填'}}}"></asp:TextBox></div>
        <div style="display: inherit" class="datepicker">
            <asp:Label ID="lblendDate" runat="server" AssociatedControlID="endDate">結束日期</asp:Label><asp:TextBox
                ID="endDate" runat="server" Width="250px" CssClass="{validate:{date:true, messages:{date:'結束日期格式不正確'}}}"></asp:TextBox>
            (不設定代表永遠生效)</div>
            <div style="display: inherit">
            <asp:Label ID="Label1" runat="server" AssociatedControlID="pdfUrl">上傳pdf檔</asp:Label><asp:DropDownList
                ID="ddlPDF" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="ddlPDF_SelectedIndexChanged">
                <asp:ListItem>檔案上傳</asp:ListItem>
                <asp:ListItem>檔案連結</asp:ListItem>
            </asp:DropDownList>
            <asp:FileUpload ID="fuPDF" runat="server" />
            <asp:TextBox ID="pdfUrl" runat="server" Width="250px" Visible="False"></asp:TextBox></div>

        
        <div style="display: inherit">
            <asp:Label ID="lblfileUrl" runat="server" AssociatedControlID="fileUrl">相關檔案</asp:Label><asp:DropDownList
                ID="ddlFile" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlFile_SelectedIndexChanged">
                <asp:ListItem>檔案上傳</asp:ListItem>
                <asp:ListItem>檔案連結</asp:ListItem>
            </asp:DropDownList>
            <asp:FileUpload ID="fuFile" runat="server" />
            <asp:TextBox ID="fileUrl" runat="server" Width="250px" Visible="False"></asp:TextBox></div>

            <div style="display: inherit">
            <asp:Label ID="lblpicUrl" runat="server" AssociatedControlID="fuPic">相關圖片</asp:Label>
            <asp:FileUpload ID="fuPic" runat="server" />
        </div>
        <div style="display: inherit">
            <asp:Label ID="lbllinkUrl" runat="server" AssociatedControlID="linkUrl">相關連結</asp:Label><asp:TextBox
                ID="linkUrl" runat="server" Width="250px"></asp:TextBox>
            <asp:DropDownList ID="linkTarget" runat="server">
                <asp:ListItem Value="_blank">另開新視窗</asp:ListItem>
                <asp:ListItem Value="_self">在當前視窗開啟</asp:ListItem>
            </asp:DropDownList>
            (外連結請加http://)
        </div>
        <div style="display: inherit">
            <asp:Label ID="lbllinkText" runat="server" AssociatedControlID="linkText">連結說明文字</asp:Label><asp:TextBox
                ID="linkText" runat="server" Width="250px"></asp:TextBox></div>
         
         
         
         </asp:Panel>
        <div>
            <asp:Button ID="InsertButton" runat="server" Text="確定" OnClick="InsertButton_Click">
            </asp:Button><input type="button" value="回上一頁" onclick="history.back()"></div>
        <div>
            <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label></div>
    </fieldset>
</asp:Content>
