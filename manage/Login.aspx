﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="manage_Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>後臺</title>
    <script src="javascript/jquery-1.5.2.min.js" type="text/javascript"></script>
    <script src="javascript/jquery.validate.js" type="text/javascript"></script>
    <link href="css_styles/layouts/login.css" rel="stylesheet" type="text/css" />
    <link href="css_styles/components/jquery_validate.css" rel="stylesheet" type="text/css" />
       <script type="text/javascript">
           $(document).ready(function () {
               $("#form1").validate({
                   errorLabelContainer: $("#form1 div.container")
               });
           });
            
    </script>
</head>
<body>


<form id="form1" runat="server">
<div id="box">
    <div class="login">
        <div class="title1">後臺</div>
        <div class="container"><h4>你送出的資料有錯誤，請檢視下列訊息</h4>
	    <ol>
		<li><label for="Username" class="error"> 請輸入帳號</label></li>
	   	<li><label for="Password" class="error"> 請輸入密碼</label></li>
	    </ol>
    </div></div>
    
        <div class="content">
<div class="centerbox">
                   
<table width="384" height="265" border="0" class="table1">
              <tr>
                <td height="111" colspan="2"><img src="images/login-3.gif" width="284" height="75" class="img2"/></td>
              </tr>
              <tr>
                <td height="41" colspan="2"><img src="images/login-4.gif" width="234" height="42" class="img3"/><br />
                <br /></td>
              </tr>
              <tr>
                <td width="134" height="27"><img src="images/loign-5.gif" width="71" height="22" class="img1"/></td>
                <td width="240">
                  <asp:TextBox ID="Username" runat="server" CssClass="required text1" >admin</asp:TextBox>
                </td>
              </tr>
              <tr>
                <td height="36"><img src="images/loign-6.gif" width="71" height="21" class="img1" /></td>
                <td>
                  <asp:TextBox ID="Password" runat="server" TextMode="Password" CssClass="required text1"></asp:TextBox>
                </td>
              </tr>
              <tr>
                <td>
                 
                </td>
                <td> <div class="button">
                    <asp:ImageButton ID="ibtnButton" runat="server" 
                    ImageUrl="images/index_04.gif" onclick="ibtnButton_Click" />                
                  </div></td>
              </tr>
            </table>
            <p class="font1">&nbsp;</p>
</div> </div>        
       
</div>
                    <div class="ErrorInfo">
                        <asp:Label ID="lblErrer" runat="server" ForeColor="Red"></asp:Label>
                    </div>
    <asp:AccessDataSource ID="AccessDataSource1" runat="server" 
    DataFile="~/App_Data/Wang.mdb" 
    SelectCommand="SELECT Account.id, Account.Account, Role.RoleName, Role.Permission, Account.[Password], Account.Cname FROM (Account INNER JOIN Role ON Account.RoleID = Role.id) WHERE (Account.Account = ?) AND (Account.[Password] = ?)">
        <SelectParameters>
            <asp:Parameter Name="?" />
            <asp:Parameter Name="?" />
        </SelectParameters>
</asp:AccessDataSource>
    </form>
</body>
</html>
