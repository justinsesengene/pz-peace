﻿<%@ Page ValidateRequest="false" Title="" Language="C#" MasterPageFile="~/manage/MasterPage.master" AutoEventWireup="true" CodeFile="welfare_Eidt.aspx.cs" Inherits="manage_human_welfare_Eidt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/jquery-1.4.2.min.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/jquery.validate.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/jquery.metadata.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/ui.datepicker.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/ui.datepicker-zh-TW.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/jquery.maxlength-min.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/thickbox.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/Scripts/ckeditor/ckeditor.js") %>"></script>
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/jquery_validate.css") %>"
        rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/ui.core.css") %>"
        rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/ui.datepicker.css") %>"
        rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/ui.theme.css") %>"
        rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/single_seventeen.css") %>"
        rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/maxlength.css") %>"
        rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/thickbox.css") %>" rel="stylesheet"
        type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () {
            $("form").validate({ meta: "validate" });
            $(".datepicker input").datepicker();
        });


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<fieldset>
<legend>編輯福利制度</legend>
    <div style="display: inherit">
            <div>
                <asp:Label ID="lblcontent" runat="server" AssociatedControlID="content">詳細內容</asp:Label>
            </div>
            <div style="clear:both">
                <asp:TextBox ID="content" runat="server" CssClass="ckeditor" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
         

<div><asp:Button ID="InsertButton" runat="server"  Text="確定" 
                 onclick="InsertButton_Click"></asp:Button><input type="button" value="回上一頁" onclick="history.back()"/></div><div>
<asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label></div>
</fieldset>
    <%--獎金福利--%>
    <fieldset>
    <legend>獎金福利</legend>
         <div>
    <a href="<%=String.Format("welfare_Content_Insert.aspx?ModuleID={0}&Type={1}&KeepThis=true&TB_iframe=true&height=200&width=600", Request["ModuleID"],"1") %>" title="獎金福利" class="thickbox" >新增獎金福利</a>
    </div>
          <div style="display: inherit">
              <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        CssClass="tableStyle1" DataKeyNames="id" 
        EnableModelValidation="True" GridLines="None" 
        onrowdeleting="GridView1_RowDeleting" 
        onrowupdating="GridView1_RowUpdating" AllowPaging="True" 
        onpageindexchanging="GridView1_PageIndexChanging" onrowdatabound="GridView1_RowDataBound">
        <Columns>
            <asp:BoundField DataField="articleTitle" HeaderText="獎金福利標題" 
                SortExpression="articleTitle" />
            <asp:BoundField DataField="listNum" HeaderText="獎金福利排序" 
                SortExpression="listNum" />

                   <asp:TemplateField HeaderText="編輯">
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkEdit" runat="server" CssClass="thickbox" >編輯</asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            <asp:CommandField ShowDeleteButton="True" HeaderText="刪除" />

        </Columns>

    </asp:GridView>
              </div>
</fieldset>
     <%--休假福利--%>
    <fieldset>
    <legend>休假福利</legend>
         <div>
    <a href="<%=String.Format("welfare_Content_Insert.aspx?ModuleID={0}&Type={1}&KeepThis=true&TB_iframe=true&height=200&width=600", Request["ModuleID"],"2") %>" title="休假福利" class="thickbox" >新增休假福利</a>
    </div>
          <div style="display: inherit">
              <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" 
        CssClass="tableStyle1" DataKeyNames="id" 
        EnableModelValidation="True" GridLines="None" 
        onrowdeleting="GridView2_RowDeleting" 
        onrowupdating="GridView2_RowUpdating" AllowPaging="True" 
        onpageindexchanging="GridView2_PageIndexChanging" onrowdatabound="GridView2_RowDataBound">
        <Columns>
            <asp:BoundField DataField="articleTitle" HeaderText="休假福利標題" 
                SortExpression="articleTitle" />
            <asp:BoundField DataField="listNum" HeaderText="休假福利排序" 
                SortExpression="listNum" />

                   <asp:TemplateField HeaderText="編輯">
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkEdit" runat="server" CssClass="thickbox" >編輯</asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            <asp:CommandField ShowDeleteButton="True" HeaderText="刪除" />

        </Columns>

    </asp:GridView>
              </div>
</fieldset>

     <%--保險福利--%>
    <fieldset>
    <legend>保險福利</legend>
         <div>
    <a href="<%=String.Format("welfare_Content_Insert.aspx?ModuleID={0}&Type={1}&KeepThis=true&TB_iframe=true&height=200&width=600", Request["ModuleID"],"3") %>" title="保險福利" class="thickbox" >新增保險福利</a>
    </div>
          <div style="display: inherit">
              <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" 
        CssClass="tableStyle1" DataKeyNames="id" 
        EnableModelValidation="True" GridLines="None" 
        onrowdeleting="GridView3_RowDeleting" 
        onrowupdating="GridView3_RowUpdating" AllowPaging="True" 
        onpageindexchanging="GridView3_PageIndexChanging" onrowdatabound="GridView3_RowDataBound">
        <Columns>
            <asp:BoundField DataField="articleTitle" HeaderText="保險福利標題" 
                SortExpression="articleTitle" />
            <asp:BoundField DataField="listNum" HeaderText="保險福利排序" 
                SortExpression="listNum" />

                   <asp:TemplateField HeaderText="編輯">
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkEdit" runat="server" CssClass="thickbox" >編輯</asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            <asp:CommandField ShowDeleteButton="True" HeaderText="刪除" />

        </Columns>

    </asp:GridView>
              </div>
</fieldset>
    <%--餐飲福利--%>
    <fieldset>
    <legend>餐飲福利</legend>
         <div>
    <a href="<%=String.Format("welfare_Content_Insert.aspx?ModuleID={0}&Type={1}&KeepThis=true&TB_iframe=true&height=200&width=600", Request["ModuleID"],"4") %>" title="餐飲福利" class="thickbox" >新增餐飲福利</a>
    </div>
          <div style="display: inherit">
              <asp:GridView ID="GridView4" runat="server" AutoGenerateColumns="False" 
        CssClass="tableStyle1" DataKeyNames="id" 
        EnableModelValidation="True" GridLines="None" 
        onrowdeleting="GridView4_RowDeleting" 
        onrowupdating="GridView4_RowUpdating" AllowPaging="True" 
        onpageindexchanging="GridView4_PageIndexChanging" onrowdatabound="GridView4_RowDataBound">
        <Columns>
            <asp:BoundField DataField="articleTitle" HeaderText="餐飲福利標題" 
                SortExpression="articleTitle" />
            <asp:BoundField DataField="listNum" HeaderText="餐飲福利排序" 
                SortExpression="listNum" />

                   <asp:TemplateField HeaderText="編輯">
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkEdit" runat="server" CssClass="thickbox" >編輯</asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            <asp:CommandField ShowDeleteButton="True" HeaderText="刪除" />

        </Columns>

    </asp:GridView>
              </div>
</fieldset>
    
    <%--衣著福利--%>
    <fieldset>
    <legend>衣著福利</legend>
         <div>
    <a href="<%=String.Format("welfare_Content_Insert.aspx?ModuleID={0}&Type={1}&KeepThis=true&TB_iframe=true&height=200&width=600", Request["ModuleID"],"5") %>" title="衣著福利" class="thickbox" >新增衣著福利</a>
    </div>
          <div style="display: inherit">
              <asp:GridView ID="GridView5" runat="server" AutoGenerateColumns="False" 
        CssClass="tableStyle1" DataKeyNames="id" 
        EnableModelValidation="True" GridLines="None" 
        onrowdeleting="GridView5_RowDeleting" 
        onrowupdating="GridView5_RowUpdating" AllowPaging="True" 
        onpageindexchanging="GridView5_PageIndexChanging" onrowdatabound="GridView5_RowDataBound">
        <Columns>
            <asp:BoundField DataField="articleTitle" HeaderText="衣著福利標題" 
                SortExpression="articleTitle" />
            <asp:BoundField DataField="listNum" HeaderText="衣著福利排序" 
                SortExpression="listNum" />

                   <asp:TemplateField HeaderText="編輯">
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkEdit" runat="server" CssClass="thickbox" >編輯</asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            <asp:CommandField ShowDeleteButton="True" HeaderText="刪除" />

        </Columns>

    </asp:GridView>
              </div>
</fieldset>
    <%--交通福利--%>
    <fieldset>
    <legend>交通福利</legend>
         <div>
    <a href="<%=String.Format("welfare_Content_Insert.aspx?ModuleID={0}&Type={1}&KeepThis=true&TB_iframe=true&height=200&width=600", Request["ModuleID"],"6") %>" title="交通福利" class="thickbox" >新增交通福利</a>
    </div>
          <div style="display: inherit">
              <asp:GridView ID="GridView6" runat="server" AutoGenerateColumns="False" 
        CssClass="tableStyle1" DataKeyNames="id" 
        EnableModelValidation="True" GridLines="None" 
        onrowdeleting="GridView6_RowDeleting" 
        onrowupdating="GridView6_RowUpdating" AllowPaging="True" 
        onpageindexchanging="GridView6_PageIndexChanging" onrowdatabound="GridView6_RowDataBound">
        <Columns>
            <asp:BoundField DataField="articleTitle" HeaderText="交通福利標題" 
                SortExpression="articleTitle" />
            <asp:BoundField DataField="listNum" HeaderText="交通福利排序" 
                SortExpression="listNum" />

                   <asp:TemplateField HeaderText="編輯">
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkEdit" runat="server" CssClass="thickbox" >編輯</asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            <asp:CommandField ShowDeleteButton="True" HeaderText="刪除" />

        </Columns>

    </asp:GridView>
              </div>
</fieldset>
    <%--娛樂福利--%>
    <fieldset>
    <legend>娛樂福利</legend>
         <div>
    <a href="<%=String.Format("welfare_Content_Insert.aspx?ModuleID={0}&Type={1}&KeepThis=true&TB_iframe=true&height=200&width=600", Request["ModuleID"],"7") %>" title="娛樂福利" class="thickbox" >新增娛樂福利</a>
    </div>
          <div style="display: inherit">
              <asp:GridView ID="GridView7" runat="server" AutoGenerateColumns="False" 
        CssClass="tableStyle1" DataKeyNames="id" 
        EnableModelValidation="True" GridLines="None" 
        onrowdeleting="GridView7_RowDeleting" 
        onrowupdating="GridView7_RowUpdating" AllowPaging="True" 
        onpageindexchanging="GridView7_PageIndexChanging" onrowdatabound="GridView7_RowDataBound">
        <Columns>
            <asp:BoundField DataField="articleTitle" HeaderText="娛樂福利標題" 
                SortExpression="articleTitle" />
            <asp:BoundField DataField="listNum" HeaderText="娛樂福利排序" 
                SortExpression="listNum" />

                   <asp:TemplateField HeaderText="編輯">
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkEdit" runat="server" CssClass="thickbox" >編輯</asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            <asp:CommandField ShowDeleteButton="True" HeaderText="刪除" />

        </Columns>

    </asp:GridView>
              </div>
</fieldset>
    <%--補助福利--%>
    <fieldset>
    <legend>補助福利</legend>
         <div>
    <a href="<%=String.Format("welfare_Content_Insert.aspx?ModuleID={0}&Type={1}&KeepThis=true&TB_iframe=true&height=200&width=600", Request["ModuleID"],"8") %>" title="補助福利" class="thickbox" >新增補助福利</a>
    </div>
          <div style="display: inherit">
              <asp:GridView ID="GridView8" runat="server" AutoGenerateColumns="False" 
        CssClass="tableStyle1" DataKeyNames="id" 
        EnableModelValidation="True" GridLines="None" 
        onrowdeleting="GridView8_RowDeleting" 
        onrowupdating="GridView8_RowUpdating" AllowPaging="True" 
        onpageindexchanging="GridView8_PageIndexChanging" onrowdatabound="GridView8_RowDataBound">
        <Columns>
            <asp:BoundField DataField="articleTitle" HeaderText="補助福利標題" 
                SortExpression="articleTitle" />
            <asp:BoundField DataField="listNum" HeaderText="補助福利排序" 
                SortExpression="listNum" />

                   <asp:TemplateField HeaderText="編輯">
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkEdit" runat="server" CssClass="thickbox" >編輯</asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            <asp:CommandField ShowDeleteButton="True" HeaderText="刪除" />

        </Columns>

    </asp:GridView>
              </div>
</fieldset>
    
    
    
    

    <asp:Panel ID="Panel1" runat="server" Visible="False">
        <div style="display: inherit">
            <asp:Label ID="lblpicUrl" runat="server" AssociatedControlID="fuPic">相關圖片</asp:Label>
            <asp:FileUpload ID="fuPic" runat="server" />
            <asp:HyperLink ID="fuPicLink" runat="server" Target="_blank">檢視圖片</asp:HyperLink>
        </div>
        <div style="display: inherit">
            <asp:Label ID="lbllinkUrl" runat="server" AssociatedControlID="linkUrl">相關連結</asp:Label><asp:TextBox
                ID="linkUrl" runat="server" Width="250px"></asp:TextBox>
            <asp:DropDownList ID="linkTarget" runat="server">
                <asp:ListItem Value="_blank">另開新視窗</asp:ListItem>
                <asp:ListItem Value="_self">在當前視窗開啟</asp:ListItem>
            </asp:DropDownList>
            (外連結請加http://)
        </div>
        <div style="display: inherit">
            <asp:Label ID="lbllinkText" runat="server" AssociatedControlID="linkText">連結說明文字</asp:Label><asp:TextBox
                ID="linkText" runat="server" Width="250px"></asp:TextBox></div>
        <div style="display:inherit;text-align: left;"><asp:label ID="lblsubject" runat="server" AssociatedControlID="subject">名稱</asp:label><asp:TextBox ID="subject" runat="server" width="250px" CssClass="{validate:{required:true, messages:{required:'標題必填'}}}"></asp:TextBox></div>
<div style="display: inherit" class="datepicker">
            <asp:Label ID="lblstartDate" runat="server" AssociatedControlID="startDate" >開始日期</asp:Label><asp:TextBox
                ID="startDate" runat="server" Width="250px" CssClass="{validate:{required:true, messages:{required:'開始日期必填'}}}"></asp:TextBox></div>
        <div style="display: inherit" class="datepicker">
            <asp:Label ID="lblendDate" runat="server" AssociatedControlID="endDate">結束日期</asp:Label><asp:TextBox
                ID="endDate" runat="server" Width="250px" CssClass="{validate:{date:true, messages:{date:'結束日期格式不正確'}}}"></asp:TextBox>
            (不設定代表永遠生效)</div>
            <div style="display: inherit">
            <asp:Label ID="Label1" runat="server" AssociatedControlID="pdfUrl">上傳pdf檔</asp:Label><asp:DropDownList
                ID="ddlPDF" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="ddlPDF_SelectedIndexChanged">
                <asp:ListItem>檔案上傳</asp:ListItem>
                <asp:ListItem Selected="True">檔案連結</asp:ListItem>
            </asp:DropDownList>
            <asp:FileUpload ID="fuPDF" runat="server" Visible="False" />
            <asp:TextBox ID="pdfUrl" runat="server" Width="250px" ></asp:TextBox>
            <asp:HyperLink ID="pdfUrlLink" runat="server" Target="_blank">下載檔案</asp:HyperLink>
            </div>

            <div style="display: inherit">
            <asp:Label ID="lblfileUrl" runat="server" AssociatedControlID="fileUrl">檔案</asp:Label><asp:DropDownList
                ID="ddlFile" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlFile_SelectedIndexChanged">
                <asp:ListItem>檔案連結</asp:ListItem>
                <asp:ListItem>檔案上傳</asp:ListItem>
            </asp:DropDownList>
            <asp:FileUpload ID="fuFile" runat="server" Visible="False" />
            <asp:TextBox ID="fileUrl" runat="server" Width="250px"></asp:TextBox>
            <asp:HyperLink ID="fileUrlLink" runat="server" Target="_blank">下載檔案</asp:HyperLink>
        </div>

        
        </asp:Panel>
</asp:Content>

