﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class manage_human_welfare_Eidt : System.Web.UI.Page
{
    #region "將目前功能資料送到Master Page裡"
    protected void Page_Init(object sender, EventArgs e)
    {

        HiddenField FunctionNumber = (HiddenField)Master.FindControl("FunctionNumber");
        FunctionNumber.Value = "H03";
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            show();
        }
    }

    private void show()
    {
        EasyDataProvide ModulePublish = new EasyDataProvide("ModulePublish");
        ModulePublish.addParameter("moduleID", Request["moduleID"]);
        DataTable dt = ModulePublish.getData("moduleID=@moduleID and version='C'");

        if (dt.Rows.Count > 0)
        {
            content.Text = dt.Rows[0]["content"].ToString();
            ViewState["ID"] = dt.Rows[0]["id"].ToString();


            DataRow row = ModulePublish.fillPageControlsByID(ViewState["ID"].ToString(), Page, "ContentPlaceHolder1");
            if (endDate.Text == "2800/1/1")
            {
                endDate.Text = "";
            }
            if (string.IsNullOrEmpty(row["fileUrl"].ToString()))
            {
                fileUrlLink.Visible = false;
            }
            else
            {
                fileUrlLink.NavigateUrl = "~/UploadFiles/Files/" + row["fileUrl"].ToString();
            }
            if (string.IsNullOrEmpty(row["pdfUrl"].ToString()))
            {
                pdfUrlLink.Visible = false;
            }
            else
            {
                pdfUrlLink.NavigateUrl = "~/UploadFiles/Files/" + row["pdfUrl"].ToString();
            }

            if (string.IsNullOrEmpty(row["picUrl"].ToString()))
            {
                fuPicLink.Visible = false;
            }
            else
            {
                fuPicLink.NavigateUrl = "~/UploadFiles/Images/" + row["picUrl"].ToString();
            }

            
            EasyDataProvide ModuleContents = new EasyDataProvide("ModuleContents");
            ModuleContents.addParameter("publishID", ViewState["ID"].ToString());
            //獎金福利
            DataTable dtCnt1 = ModuleContents.getData("publishID=@publishID and type=1", "order by listNum ASC");
            if (dtCnt1.Rows.Count > 0)
            {
                GridView1.DataSource = dtCnt1;
                GridView1.DataBind();
            }
            //休假福利
            DataTable dtCnt2 = ModuleContents.getData("publishID=@publishID and type=2", "order by listNum ASC");
            if (dtCnt2.Rows.Count > 0)
            {
                GridView2.DataSource = dtCnt2;
                GridView2.DataBind();
            }
            //保險福利
            DataTable dtCnt3 = ModuleContents.getData("publishID=@publishID and type=3", "order by listNum ASC");
            if (dtCnt3.Rows.Count > 0)
            {
                GridView3.DataSource = dtCnt3;
                GridView3.DataBind();
            }
            //餐飲福利
            DataTable dtCnt4 = ModuleContents.getData("publishID=@publishID and type=4", "order by listNum ASC");
            if (dtCnt4.Rows.Count > 0)
            {
                GridView4.DataSource = dtCnt4;
                GridView4.DataBind();
            }
            //衣著福利
            DataTable dtCnt5 = ModuleContents.getData("publishID=@publishID and type=5", "order by listNum ASC");
            if (dtCnt5.Rows.Count > 0)
            {
                GridView5.DataSource = dtCnt5;
                GridView5.DataBind();
            }
            //交通福利
            DataTable dtCnt6 = ModuleContents.getData("publishID=@publishID and type=6", "order by listNum ASC");
            if (dtCnt6.Rows.Count > 0)
            {
                GridView6.DataSource = dtCnt6;
                GridView6.DataBind();
            }
            //娛樂福利
            DataTable dtCnt7 = ModuleContents.getData("publishID=@publishID and type=7", "order by listNum ASC");
            if (dtCnt7.Rows.Count > 0)
            {
                GridView7.DataSource = dtCnt7;
                GridView7.DataBind();
            }
            //補助福利
            DataTable dtCnt8 = ModuleContents.getData("publishID=@publishID and type=8", "order by listNum ASC");
            if (dtCnt8.Rows.Count > 0)
            {
                GridView8.DataSource = dtCnt8;
                GridView8.DataBind();
            }
        }
    }
    //獎金福利
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[3].Attributes.Add("onclick",
                                          "javascript:if(!window.confirm('你確定刪除嗎?')) return false;");
            string picID = GridView1.DataKeys[e.Row.RowIndex].Value.ToString();
            HyperLink lnkEdit = (HyperLink)e.Row.FindControl("lnkEdit");
            lnkEdit.NavigateUrl =
                String.Format(
                    "welfare_Content_Edit.aspx?ModuleID={0}&ID={1}&KeepThis=true&TB_iframe=true&height=200&width=600",
                    Request["ModuleID"], picID);
        }
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string strID = GridView1.DataKeys[e.RowIndex].Value.ToString();
        EasyDataProvide ModuleContents = new EasyDataProvide("ModuleContents");
        ModuleContents.DeleteById(strID);

        //取得寫入LOG
        EasyDataProvide Log = new EasyDataProvide("Log");
        string[] strUserData = ((FormsIdentity)(Page.User.Identity)).Ticket.UserData.Split(new Char[] { ';' });
        Log.addParameter("poster", strUserData[2]);
        Log.addParameter("subject", "刪除一筆獎金福利");
        Log.addParameter("version", "C");
        Log.addParameter("moduleID", "H03");
        Log.Insert();
        Response.Redirect("welfare_Eidt.aspx?ModuleID=" + Request["ModuleID"]);

        show();
    }
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string strID = GridView1.DataKeys[e.RowIndex].Value.ToString();
        string page = Request["page"] ?? "1";
        //Response.Redirect("milestone_Edit.aspx?ID=" + strID + "&ModuleID=" + Request["ModuleID"] + "&page=" + page);
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        show();
    }
    //休假福利
    protected void GridView2_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string strID = GridView2.DataKeys[e.RowIndex].Value.ToString();
        string page = Request["page"] ?? "1";
        //Response.Redirect("milestone_Edit.aspx?ID=" + strID + "&ModuleID=" + Request["ModuleID"] + "&page=" + page);
    }
    protected void GridView2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView2.PageIndex = e.NewPageIndex;
        show();
    }
    protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[3].Attributes.Add("onclick",
                                          "javascript:if(!window.confirm('你確定刪除嗎?')) return false;");
            string picID = GridView2.DataKeys[e.Row.RowIndex].Value.ToString();
            HyperLink lnkEdit = (HyperLink)e.Row.FindControl("lnkEdit");
            lnkEdit.NavigateUrl =
                String.Format(
                    "welfare_Content_Edit.aspx?ModuleID={0}&ID={1}&KeepThis=true&TB_iframe=true&height=200&width=600",
                    Request["ModuleID"], picID);
        }
    }
    protected void GridView2_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string strID = GridView2.DataKeys[e.RowIndex].Value.ToString();
        EasyDataProvide ModuleContents = new EasyDataProvide("ModuleContents");
        ModuleContents.DeleteById(strID);

        //取得寫入LOG
        EasyDataProvide Log = new EasyDataProvide("Log");
        string[] strUserData = ((FormsIdentity)(Page.User.Identity)).Ticket.UserData.Split(new Char[] { ';' });
        Log.addParameter("poster", strUserData[2]);
        Log.addParameter("subject", "刪除一筆休假福利");
        Log.addParameter("version", "C");
        Log.addParameter("moduleID", "H03");
        Log.Insert();
        Response.Redirect("welfare_Eidt.aspx?ModuleID=" + Request["ModuleID"]);

        show();
    }
    //保險福利
    protected void GridView3_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[3].Attributes.Add("onclick",
                                          "javascript:if(!window.confirm('你確定刪除嗎?')) return false;");
            string picID = GridView3.DataKeys[e.Row.RowIndex].Value.ToString();
            HyperLink lnkEdit = (HyperLink)e.Row.FindControl("lnkEdit");
            lnkEdit.NavigateUrl =
                String.Format(
                    "welfare_Content_Edit.aspx?ModuleID={0}&ID={1}&KeepThis=true&TB_iframe=true&height=200&width=600",
                    Request["ModuleID"], picID);
        }
    }
    protected void GridView3_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string strID = GridView3.DataKeys[e.RowIndex].Value.ToString();
        EasyDataProvide ModuleContents = new EasyDataProvide("ModuleContents");
        ModuleContents.DeleteById(strID);

        //取得寫入LOG
        EasyDataProvide Log = new EasyDataProvide("Log");
        string[] strUserData = ((FormsIdentity)(Page.User.Identity)).Ticket.UserData.Split(new Char[] { ';' });
        Log.addParameter("poster", strUserData[2]);
        Log.addParameter("subject", "刪除一筆保險福利");
        Log.addParameter("version", "C");
        Log.addParameter("moduleID", "H03");
        Log.Insert();
        Response.Redirect("welfare_Eidt.aspx?ModuleID=" + Request["ModuleID"]);

        show();
    }
    protected void GridView3_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string strID = GridView3.DataKeys[e.RowIndex].Value.ToString();
        string page = Request["page"] ?? "1";
        //Response.Redirect("milestone_Edit.aspx?ID=" + strID + "&ModuleID=" + Request["ModuleID"] + "&page=" + page);
    }
    protected void GridView3_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView3.PageIndex = e.NewPageIndex;
        show();
    }
    //餐飲福利
    protected void GridView4_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[3].Attributes.Add("onclick",
                                          "javascript:if(!window.confirm('你確定刪除嗎?')) return false;");
            string picID = GridView4.DataKeys[e.Row.RowIndex].Value.ToString();
            HyperLink lnkEdit = (HyperLink)e.Row.FindControl("lnkEdit");
            lnkEdit.NavigateUrl =
                String.Format(
                    "welfare_Content_Edit.aspx?ModuleID={0}&ID={1}&KeepThis=true&TB_iframe=true&height=200&width=600",
                    Request["ModuleID"], picID);
        }
    }
    protected void GridView4_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string strID = GridView4.DataKeys[e.RowIndex].Value.ToString();
        EasyDataProvide ModuleContents = new EasyDataProvide("ModuleContents");
        ModuleContents.DeleteById(strID);

        //取得寫入LOG
        EasyDataProvide Log = new EasyDataProvide("Log");
        string[] strUserData = ((FormsIdentity)(Page.User.Identity)).Ticket.UserData.Split(new Char[] { ';' });
        Log.addParameter("poster", strUserData[2]);
        Log.addParameter("subject", "刪除一筆餐飲福利");
        Log.addParameter("version", "C");
        Log.addParameter("moduleID", "H03");
        Log.Insert();
        Response.Redirect("welfare_Eidt.aspx?ModuleID=" + Request["ModuleID"]);

        show();
    }
    protected void GridView4_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string strID = GridView4.DataKeys[e.RowIndex].Value.ToString();
        string page = Request["page"] ?? "1";
        //Response.Redirect("milestone_Edit.aspx?ID=" + strID + "&ModuleID=" + Request["ModuleID"] + "&page=" + page);
    }
    protected void GridView4_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView4.PageIndex = e.NewPageIndex;
        show();
    }
    //衣著福利
    protected void GridView5_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[3].Attributes.Add("onclick",
                                          "javascript:if(!window.confirm('你確定刪除嗎?')) return false;");
            string picID = GridView5.DataKeys[e.Row.RowIndex].Value.ToString();
            HyperLink lnkEdit = (HyperLink)e.Row.FindControl("lnkEdit");
            lnkEdit.NavigateUrl =
                String.Format(
                    "welfare_Content_Edit.aspx?ModuleID={0}&ID={1}&KeepThis=true&TB_iframe=true&height=200&width=600",
                    Request["ModuleID"], picID);
        }
    }
    protected void GridView5_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string strID = GridView5.DataKeys[e.RowIndex].Value.ToString();
        EasyDataProvide ModuleContents = new EasyDataProvide("ModuleContents");
        ModuleContents.DeleteById(strID);

        //取得寫入LOG
        EasyDataProvide Log = new EasyDataProvide("Log");
        string[] strUserData = ((FormsIdentity)(Page.User.Identity)).Ticket.UserData.Split(new Char[] { ';' });
        Log.addParameter("poster", strUserData[2]);
        Log.addParameter("subject", "刪除一筆衣著福利");
        Log.addParameter("version", "C");
        Log.addParameter("moduleID", "H03");
        Log.Insert();
        Response.Redirect("welfare_Eidt.aspx?ModuleID=" + Request["ModuleID"]);

        show();
    }
    protected void GridView5_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string strID = GridView5.DataKeys[e.RowIndex].Value.ToString();
        string page = Request["page"] ?? "1";
        //Response.Redirect("milestone_Edit.aspx?ID=" + strID + "&ModuleID=" + Request["ModuleID"] + "&page=" + page);
    }
    protected void GridView5_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView5.PageIndex = e.NewPageIndex;
        show();
    }
    //交通福利
    protected void GridView6_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[3].Attributes.Add("onclick",
                                          "javascript:if(!window.confirm('你確定刪除嗎?')) return false;");
            string picID = GridView6.DataKeys[e.Row.RowIndex].Value.ToString();
            HyperLink lnkEdit = (HyperLink)e.Row.FindControl("lnkEdit");
            lnkEdit.NavigateUrl =
                String.Format(
                    "welfare_Content_Edit.aspx?ModuleID={0}&ID={1}&KeepThis=true&TB_iframe=true&height=200&width=600",
                    Request["ModuleID"], picID);
        }
    }
    protected void GridView6_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string strID = GridView6.DataKeys[e.RowIndex].Value.ToString();
        EasyDataProvide ModuleContents = new EasyDataProvide("ModuleContents");
        ModuleContents.DeleteById(strID);

        //取得寫入LOG
        EasyDataProvide Log = new EasyDataProvide("Log");
        string[] strUserData = ((FormsIdentity)(Page.User.Identity)).Ticket.UserData.Split(new Char[] { ';' });
        Log.addParameter("poster", strUserData[2]);
        Log.addParameter("subject", "刪除一筆交通福利");
        Log.addParameter("version", "C");
        Log.addParameter("moduleID", "H03");
        Log.Insert();
        Response.Redirect("welfare_Eidt.aspx?ModuleID=" + Request["ModuleID"]);

        show();
    }
    protected void GridView6_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string strID = GridView6.DataKeys[e.RowIndex].Value.ToString();
        string page = Request["page"] ?? "1";
        //Response.Redirect("milestone_Edit.aspx?ID=" + strID + "&ModuleID=" + Request["ModuleID"] + "&page=" + page);
    }
    protected void GridView6_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView6.PageIndex = e.NewPageIndex;
        show();
    }
    //娛樂福利
    protected void GridView7_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[3].Attributes.Add("onclick",
                                          "javascript:if(!window.confirm('你確定刪除嗎?')) return false;");
            string picID = GridView7.DataKeys[e.Row.RowIndex].Value.ToString();
            HyperLink lnkEdit = (HyperLink)e.Row.FindControl("lnkEdit");
            lnkEdit.NavigateUrl =
                String.Format(
                    "welfare_Content_Edit.aspx?ModuleID={0}&ID={1}&KeepThis=true&TB_iframe=true&height=200&width=600",
                    Request["ModuleID"], picID);
        }
    }
    protected void GridView7_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string strID = GridView7.DataKeys[e.RowIndex].Value.ToString();
        EasyDataProvide ModuleContents = new EasyDataProvide("ModuleContents");
        ModuleContents.DeleteById(strID);

        //取得寫入LOG
        EasyDataProvide Log = new EasyDataProvide("Log");
        string[] strUserData = ((FormsIdentity)(Page.User.Identity)).Ticket.UserData.Split(new Char[] { ';' });
        Log.addParameter("poster", strUserData[2]);
        Log.addParameter("subject", "刪除一筆娛樂福利");
        Log.addParameter("version", "C");
        Log.addParameter("moduleID", "H03");
        Log.Insert();
        Response.Redirect("welfare_Eidt.aspx?ModuleID=" + Request["ModuleID"]);

        show();
    }
    protected void GridView7_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string strID = GridView7.DataKeys[e.RowIndex].Value.ToString();
        string page = Request["page"] ?? "1";
        //Response.Redirect("milestone_Edit.aspx?ID=" + strID + "&ModuleID=" + Request["ModuleID"] + "&page=" + page);
    }
    protected void GridView7_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView7.PageIndex = e.NewPageIndex;
        show();
    }
    //補助福利
    protected void GridView8_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[3].Attributes.Add("onclick",
                                          "javascript:if(!window.confirm('你確定刪除嗎?')) return false;");
            string picID = GridView8.DataKeys[e.Row.RowIndex].Value.ToString();
            HyperLink lnkEdit = (HyperLink)e.Row.FindControl("lnkEdit");
            lnkEdit.NavigateUrl =
                String.Format(
                    "welfare_Content_Edit.aspx?ModuleID={0}&ID={1}&KeepThis=true&TB_iframe=true&height=200&width=600",
                    Request["ModuleID"], picID);
        }
    }
    protected void GridView8_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string strID = GridView8.DataKeys[e.RowIndex].Value.ToString();
        EasyDataProvide ModuleContents = new EasyDataProvide("ModuleContents");
        ModuleContents.DeleteById(strID);

        //取得寫入LOG
        EasyDataProvide Log = new EasyDataProvide("Log");
        string[] strUserData = ((FormsIdentity)(Page.User.Identity)).Ticket.UserData.Split(new Char[] { ';' });
        Log.addParameter("poster", strUserData[2]);
        Log.addParameter("subject", "刪除一筆補助福利");
        Log.addParameter("version", "C");
        Log.addParameter("moduleID", "H03");
        Log.Insert();
        Response.Redirect("welfare_Eidt.aspx?ModuleID=" + Request["ModuleID"]);

        show();
    }
    protected void GridView8_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string strID = GridView8.DataKeys[e.RowIndex].Value.ToString();
        string page = Request["page"] ?? "1";
        //Response.Redirect("milestone_Edit.aspx?ID=" + strID + "&ModuleID=" + Request["ModuleID"] + "&page=" + page);
    }
    protected void GridView8_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView8.PageIndex = e.NewPageIndex;
        show();
    }




    protected void InsertButton_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(content.Text))
        {
            lblError.Text = "請輸入資料";
            return;
        }
        EasyDataProvide ModulePublish = new EasyDataProvide("ModulePublish");

        if (ViewState["ID"] != null)
        {

            ModulePublish.setPageFormQuest(Page, "ContentPlaceHolder1");
            //不填結束日期時，設定一個800年後的日期
            if (string.IsNullOrEmpty(endDate.Text))
            {
                ModulePublish.addParameter("endDate", "2800/1/1");
            }
            //處理上傳檔案
            if (ddlFile.SelectedValue == "檔案上傳" && fuFile.HasFile)
            {
                //取得副檔名
                string Extension = fuFile.FileName.Split('.')[fuFile.FileName.Split('.').Length - 1];
                //新檔案名稱
                string fileName = String.Format("{0:yyyyMMddhhmmsss}.{1}", DateTime.Now, Extension);
                fuFile.SaveAs(Server.MapPath(String.Format("~/UploadFiles/Files/{0}", fileName)));
                ModulePublish.addParameter("fileUrl", fileName);

            }
            System.Threading.Thread.Sleep(1000);
            //處理上傳PDF檔案
            if (ddlPDF.SelectedValue == "檔案上傳" && fuPDF.HasFile)
            {
                //取得副檔名
                string Extension = fuPDF.FileName.Split('.')[fuPDF.FileName.Split('.').Length - 1];
                if (Extension.ToLower() != "pdf")
                {
                    My.WebForm.doJavaScript("alert('pdf檔錯誤!');", Page);
                    return;
                }
                //新檔案名稱
                string fileName = String.Format("{0:yyyyMMddhhmmsss}.{1}", DateTime.Now, Extension);
                fuPDF.SaveAs(Server.MapPath(String.Format("~/UploadFiles/Files/{0}", fileName)));
                ModulePublish.addParameter("pdfUrl", fileName);

            }
            //處理上傳圖片

            if (fuPic.HasFile)
            {
                if (fuPic.PostedFile.ContentType.IndexOf("image") == -1)
                {
                    My.WebForm.doJavaScript("alert('檔案型態錯誤!');", Page);
                    return;
                }

                //取得副檔名
                string Extension = fuPic.FileName.Split('.')[fuPic.FileName.Split('.').Length - 1];
                //新檔案名稱
                string fileName = String.Format("{0:yyyyMMddhhmmsss}.{1}", DateTime.Now, Extension);
                fuPic.SaveAs(Server.MapPath(String.Format("~/UploadFiles/Images/{0}", fileName)));
                ModulePublish.addParameter("picUrl", fileName);

            }
            ModulePublish.UpdateById(ViewState["ID"].ToString());

            //取得寫入LOG
            EasyDataProvide Log = new EasyDataProvide("Log");
            string[] strUserData = ((FormsIdentity)(Page.User.Identity)).Ticket.UserData.Split(new Char[] { ';' });
            Log.addParameter("poster", strUserData[2]);
            Log.addParameter("subject", "編輯一筆資料");
            Log.addParameter("version", "C");
            Log.addParameter("moduleID", "H03");
            Log.Insert();


        }
        else
        {
            ModulePublish.addParameter("id", Guid.NewGuid().ToString());
            ModulePublish.setPageFormQuest(Page, "ContentPlaceHolder1");
            ModulePublish.addParameter("version", "C");
            ModulePublish.addParameter("moduleID", "H03");
            ModulePublish.Insert();
        }
        Response.Redirect("welfare_Eidt.aspx?ModuleID=" + "H03");
    }
    protected void ddlFile_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlFile.SelectedValue == "檔案上傳")
        {
            fuFile.Visible = true;
            fileUrl.Visible = false;
        }
        else
        {
            fuFile.Visible = false;
            fileUrl.Visible = true;
        }
    }
    protected void ddlPDF_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlPDF.SelectedValue == "檔案上傳")
        {
            fuPDF.Visible = true;
            pdfUrl.Visible = false;
        }
        else
        {
            fuPDF.Visible = false;
            pdfUrl.Visible = true;
        }
    }
}