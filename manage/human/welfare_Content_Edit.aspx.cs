﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class manage_human_welfare_Content_Edit : System.Web.UI.Page
{
    private EasyDataProvide _ModuleContents = new EasyDataProvide("ModuleContents");
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            show();
        }
    }
    private void show()
    {
        DataRow row = _ModuleContents.fillPageControlsByID(Request["ID"].ToString(), Page);
        ViewState["type"] = row["type"].ToString();

    }
    protected void btnEdit_Click(object sender, EventArgs e)
    {
        _ModuleContents.setPageFormQuest(Page);
        

        _ModuleContents.UpdateById(Request["ID"]);

        //取得寫入LOG
        EasyDataProvide Log = new EasyDataProvide("Log");
        string[] strUserData = ((FormsIdentity)(Page.User.Identity)).Ticket.UserData.Split(new Char[] { ';' });
        Log.addParameter("poster", strUserData[2]);
        switch (ViewState["type"].ToString())
        {
            case "1":
                Log.addParameter("subject", "編輯一筆獎金福利");
                break;
            case "2":
                Log.addParameter("subject", "編輯一筆休假福利");
                break;
            case "3":
                Log.addParameter("subject", "編輯一筆保險福利");
                break;
            case "4":
                Log.addParameter("subject", "編輯一筆餐飲福利");
                break;
            case "5":
                Log.addParameter("subject", "編輯一筆衣著福利");
                break;
            case "6":
                Log.addParameter("subject", "編輯一筆交通福利");
                break;
            case "7":
                Log.addParameter("subject", "編輯一筆娛樂福利");
                break;
            case "8":
                Log.addParameter("subject", "編輯一筆補助福利");
                break;
            default:
                Log.addParameter("subject", "無");
                return;
        }
        Log.addParameter("version", "C");
        Log.addParameter("moduleID", "H03");
        Log.Insert();

        string Publish = "welfare_Eidt.aspx?ModuleID=" + "H03";
        My.WebForm.doJavaScript(String.Format("parent.tb_remove();parent.location='{0}';", Publish), Page);

    }
}