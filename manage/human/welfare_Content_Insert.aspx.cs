﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class manage_human_welfare_Content_Insert : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnADD_Click(object sender, EventArgs e)
    {
        EasyDataProvide ModulePublish = new EasyDataProvide("ModulePublish");
        ModulePublish.addParameter("moduleID", Request["moduleID"]);
        DataTable dt = ModulePublish.getData("moduleID=@moduleID and version='C'");

        if (dt.Rows.Count > 0)
        {
            ViewState["ID"] = dt.Rows[0]["id"].ToString();
        }

        EasyDataProvide ModuleContents = new EasyDataProvide("ModuleContents");
       
        try
        {
            ModuleContents.setPageFormQuest(Page);
        }
        catch (Exception ex1)
        {
            lblError.Text = ex1.Message;
            return;
        }

        ModuleContents.addParameter("publishID", ViewState["ID"].ToString());
        ModuleContents.addParameter("type", Request["Type"]);
        ModuleContents.Insert();

        //取得寫入LOG
        EasyDataProvide Log = new EasyDataProvide("Log");
        string[] strUserData = ((FormsIdentity)(Page.User.Identity)).Ticket.UserData.Split(new Char[] { ';' });
        Log.addParameter("poster", strUserData[2]);
        switch (Request["Type"])
        {
            case "1":
                Log.addParameter("subject", "新增一筆獎金福利");
                break;
            case "2":
                Log.addParameter("subject", "新增一筆休假福利");
                break;
            case "3":
                Log.addParameter("subject", "新增一筆保險福利");
                break;
            case "4":
                Log.addParameter("subject", "新增一筆餐飲福利");
                break;
            case "5":
                Log.addParameter("subject", "新增一筆衣著福利");
                break;
            case "6":
                Log.addParameter("subject", "新增一筆交通福利");
                break;
            case "7":
                Log.addParameter("subject", "新增一筆娛樂福利");
                break;
            case "8":
                Log.addParameter("subject", "新增一筆補助福利");
                break;
                default:
                Log.addParameter("subject", "無");
                return;
        }
        
        Log.addParameter("version", "C");
        Log.addParameter("moduleID", "H03");
        Log.Insert();


        string Publish = "welfare_Eidt.aspx?ModuleID=" + "H03";
        My.WebForm.doJavaScript(String.Format("parent.tb_remove();parent.location='{0}';", Publish), Page);
    }
}