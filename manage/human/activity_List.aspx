﻿<%@ Page Title="" Language="C#" MasterPageFile="~/manage/MasterPage.master" AutoEventWireup="true" CodeFile="activity_List.aspx.cs" Inherits="manage_human_activity_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div>
      <input onclick="location = 'activity_Insert.aspx'" type="button" value="新增活動花絮" class="Addbutton" />
       <div style="clear:both"></div>
      </div>

    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        CssClass="tableStyle1" DataKeyNames="id" 
        EnableModelValidation="True" GridLines="None" 
        onrowdeleting="GridView1_RowDeleting" 
        onrowupdating="GridView1_RowUpdating" AllowPaging="True" 
        onpageindexchanging="GridView1_PageIndexChanging">
        <Columns>
            <asp:BoundField DataField="subject" HeaderText="名稱" 
                SortExpression="subject" />
            <asp:TemplateField HeaderText="類別" SortExpression="version">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("classID").ToString() =="5"?"照片":"影片" %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            <asp:BoundField DataField="initDate" DataFormatString="{0:d}" HeaderText="建立日期" 
                SortExpression="initDate" />
                <asp:TemplateField HeaderText="編輯">
                    <ItemTemplate>
                        <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="編輯" 
                            CommandName="Update" ImageUrl="../images/Modify.gif" />
                    </ItemTemplate>
                   
                </asp:TemplateField>
                <asp:TemplateField HeaderText="刪除" ShowHeader="False">
                    <ItemTemplate>
                        <asp:ImageButton ID="ibtnDelete" runat="server" CssClass="action"  CommandName="Delete" ImageUrl="../images/Delete.gif" OnClientClick="javascript:if(!window.confirm('你確定刪除嗎?')) return false;" />
                    </ItemTemplate>
                    
             </asp:TemplateField>
        </Columns>

    </asp:GridView>
    </asp:Content>

