﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class manage_product_product_List : System.Web.UI.Page
{
    #region "將目前功能資料送到Master Page裡"
    protected void Page_Init(object sender, EventArgs e)
    {

        HiddenField FunctionNumber = (HiddenField)Master.FindControl("FunctionNumber");
        FunctionNumber.Value = "G02";
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            showClass();
            show();
        }
    }
    private void showClass()
    {
        EasyDataProvide ProductClass = new EasyDataProvide("ProductClass");
        ProductClass.addParameter("moduleID", "G02");
        DataTable dtProductClass = ProductClass.getData(String.Format("version='C' and moduleID=?"), "order by listNum asc");
        ddlClass.DataTextField = "className";
        ddlClass.DataValueField = "id";
        ddlClass.DataSource = dtProductClass;
        ddlClass.DataBind();
        //ListItem item = new ListItem("全部", "");
        //ddlClass.Items.Insert(0, item);
    }
    private void show()
    {
        EasyDataProvide Product = new EasyDataProvide("Product");
        Product.addParameter("moduleID", "G02");
        Product.addParameter("classID", ddlClass.SelectedValue);
        DataTable dataTable = Product.getData(String.Format("version='C' and moduleID=? and classID=?"), "order by initDate desc");
        GridView1.DataSource = dataTable;
        GridView1.DataBind();
    }
    protected void ddlClass_SelectedIndexChanged(object sender, EventArgs e)
    {
        show();
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string strID = GridView1.DataKeys[e.RowIndex].Value.ToString();
        EasyDataProvide Product = new EasyDataProvide("Product");
        Product.DeleteById(strID);

        //取得寫入LOG
        EasyDataProvide Log = new EasyDataProvide("Log");
        string[] strUserData = ((FormsIdentity)(Page.User.Identity)).Ticket.UserData.Split(new Char[] { ';' });
        Log.addParameter("poster", strUserData[2]);
        Log.addParameter("subject", "刪除一筆資料");
        Log.addParameter("version", "C");
        Log.addParameter("moduleID", "G02");
        Log.Insert();
        show();
    }
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string strID = GridView1.DataKeys[e.RowIndex].Value.ToString();
        string page = Request["page"] ?? "1";
        Response.Redirect("product_Edit.aspx?ID=" + strID + "&ModuleID=" + Request["ModuleID"] + "&page=" + page);
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        show();
    }
}