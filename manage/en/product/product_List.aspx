﻿<%@ Page Title="" Language="C#" MasterPageFile="~/manage/MasterPage.master" AutoEventWireup="true" CodeFile="product_List.aspx.cs" Inherits="manage_product_product_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div>
      <input onclick="location = 'product_Insert.aspx'" type="button" value="新增產品" class="Addbutton" />

       <div style="clear:both"></div>
      </div>
    <div style="display: inherit">
             <asp:Label ID="Label4" runat="server" AssociatedControlID="ddlClass">類別</asp:Label>
                <asp:DropDownList ID="ddlClass" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlClass_SelectedIndexChanged"></asp:DropDownList>
        </div>
        <div style="display: inherit">
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        CssClass="tableStyle1" DataKeyNames="id" 
        EnableModelValidation="True" GridLines="None" 
        onrowdeleting="GridView1_RowDeleting" 
        onrowupdating="GridView1_RowUpdating" AllowPaging="True" 
        onpageindexchanging="GridView1_PageIndexChanging">
        <Columns>
            <asp:BoundField DataField="subject" HeaderText="名稱" 
                SortExpression="subject" />
                        <asp:TemplateField HeaderText="圖片檢視" SortExpression="version">
                    <ItemTemplate>
                        <asp:HyperLink ID="fuPicLink" runat="server" Target="_blank" NavigateUrl='<%# "~/UploadFiles/Images/"+Eval("picUrl") %>'>檢視圖片</asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            <asp:BoundField DataField="initDate" DataFormatString="{0:d}" HeaderText="建立日期" 
                SortExpression="initDate" />
                <asp:TemplateField HeaderText="編輯">
                    <ItemTemplate>
                        <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="編輯" 
                            CommandName="Update" ImageUrl="../images/Modify.gif" />
                    </ItemTemplate>
                   
                </asp:TemplateField>
                <asp:TemplateField HeaderText="刪除" ShowHeader="False">
                    <ItemTemplate>
                        <asp:ImageButton ID="ibtnDelete" runat="server" CssClass="action"  CommandName="Delete" ImageUrl="../images/Delete.gif" OnClientClick="javascript:if(!window.confirm('你確定刪除嗎?')) return false;" />
                    </ItemTemplate>
                    
             </asp:TemplateField>
        </Columns>

    </asp:GridView>
            </div>
    </asp:Content>


