﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class manage_product_product_Introduce_Picture_Insert : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnADD_Click(object sender, EventArgs e)
    {
        EasyDataProvide ModulePublish = new EasyDataProvide("ModulePublish");
        ModulePublish.addParameter("moduleID", Request["moduleID"]);
        DataTable dt = ModulePublish.getData("moduleID=@moduleID and version='E'");

        if (dt.Rows.Count > 0)
        {
            ViewState["ID"] = dt.Rows[0]["id"].ToString();
        }

        EasyDataProvide ModulePictures = new EasyDataProvide("ModulePictures");
        FileUploadSetup fus = new FileUploadSetup();
        fus.name = "picUrl";
        fus.fileType = FileUploadSetup.UpfileType.Image;
        fus.allowNoFile = false;


        ThumbnailImage timg = new ThumbnailImage();
        timg.suffix = "s";
        timg.maxWidth = 153;
        timg.MaxHight = 112;

        fus.ThumbnailImages.Add(timg);
        ModulePictures.FileUploadSetups.Add(fus);
        try
        {
            ModulePictures.setPageFormQuest(Page);
        }
        catch (Exception ex1)
        {
            lblError.Text = ex1.Message;
            return;
        }

        ModulePictures.addParameter("publishID", ViewState["ID"].ToString());

        ModulePictures.Insert();

        //取得寫入LOG
        EasyDataProvide Log = new EasyDataProvide("Log");
        string[] strUserData = ((FormsIdentity)(Page.User.Identity)).Ticket.UserData.Split(new Char[] { ';' });
        Log.addParameter("poster", strUserData[2]);
        Log.addParameter("subject", "新增一張照片");
        Log.addParameter("version", "E");
        Log.addParameter("moduleID", "g01");
        Log.Insert();


        string Publish = "product_Introduce.aspx?ModuleID=" + Request["ModuleID"];
        My.WebForm.doJavaScript(String.Format("parent.tb_remove();parent.location='{0}';", Publish), Page);
    }
}