﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class manage_product_product_Introduce_Picture_Edit : System.Web.UI.Page
{
    private EasyDataProvide _ModulePictures = new EasyDataProvide("ModulePictures");
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            show();
        }
    }
    private void show()
    {
        DataRow row = _ModulePictures.fillPageControlsByID(Request["ID"].ToString(), Page);
        fuPicLink.NavigateUrl = "~/UploadFiles/Images/" + row["picUrl"].ToString();

    }
    protected void btnEdit_Click(object sender, EventArgs e)
    {
        _ModulePictures.setPageFormQuest(Page);
        if (fuPic.HasFile)
        {
            if (fuPic.PostedFile.ContentType.IndexOf("image") == -1)
            {
                My.WebForm.doJavaScript("alert('檔案型態錯誤!');", Page);
                return;
            }

            //取得副檔名
            string Extension = fuPic.FileName.Split('.')[fuPic.FileName.Split('.').Length - 1];
            //新檔案名稱
            string fileName = String.Format("{0:yyyyMMddhhmmsss}.{1}", DateTime.Now, Extension);
            fuPic.SaveAs(Server.MapPath(String.Format("~/UploadFiles/Images/{0}", fileName)));
            _ModulePictures.addParameter("picUrl", fileName);
            //產生縮圖

            My.WebForm.GenerateThumbnailImage(fileName, fuPic.PostedFile.InputStream, Server.MapPath("~/UploadFiles/Images/"), "s", 153, 112);

        }

        _ModulePictures.UpdateById(Request["ID"]);

        //取得寫入LOG
        EasyDataProvide Log = new EasyDataProvide("Log");
        string[] strUserData = ((FormsIdentity)(Page.User.Identity)).Ticket.UserData.Split(new Char[] { ';' });
        Log.addParameter("poster", strUserData[2]);
        Log.addParameter("subject", "編輯一張照片");
        Log.addParameter("version", "E");
        Log.addParameter("moduleID", "g01");
        Log.Insert();

        string Publish = "product_Introduce.aspx?ModuleID=" + Request["ModuleID"];
        My.WebForm.doJavaScript(String.Format("parent.tb_remove();parent.location='{0}';", Publish), Page);

    }
}