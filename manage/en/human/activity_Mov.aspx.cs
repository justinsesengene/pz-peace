﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class manage_human_activity_Mov : System.Web.UI.Page
{
    #region "將目前功能資料送到Master Page裡"
    protected void Page_Init(object sender, EventArgs e)
    {

        HiddenField FunctionNumber = (HiddenField)Master.FindControl("FunctionNumber");
        FunctionNumber.Value = "h04";
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
        }
    }

   
    protected void InsertButton_Click(object sender, EventArgs e)
    {


        EasyDataProvide ModulePublish = new EasyDataProvide("ModulePublish");
        //ModulePublish.setPageFormQuest(Page, "ContentPlaceHolder1");
        //不填結束日期時，設定一個800年後的日期
        ModulePublish.addParameter("default1", default1.Text);
        ModulePublish.addParameter("youTubeCode", youTubeCode.Text);
        ModulePublish.UpdateById(Request["ID"]);
        //取得寫入LOG
        EasyDataProvide Log = new EasyDataProvide("Log");
        string[] strUserData = ((FormsIdentity)(Page.User.Identity)).Ticket.UserData.Split(new Char[] { ';' });
        Log.addParameter("poster", strUserData[2]);
        Log.addParameter("subject", "新增活動影片");
        Log.addParameter("version", "E");
        Log.addParameter("moduleID", "h04");
        Log.Insert();

        Response.Redirect("activity_List.aspx?ModuleID=" + "h04");
    }


}