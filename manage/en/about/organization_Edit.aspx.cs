﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class manage_about_organization_Edit : System.Web.UI.Page
{
    #region "將目前功能資料送到Master Page裡"
    protected void Page_Init(object sender, EventArgs e)
    {

        HiddenField FunctionNumber = (HiddenField)Master.FindControl("FunctionNumber");
        FunctionNumber.Value = "d03";
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            show();
        }
    }

    private void show()
    {
        EasyDataProvide ModulePublish = new EasyDataProvide("ModulePublish");
        ModulePublish.addParameter("moduleID", Request["moduleID"]);
        DataTable dt = ModulePublish.getData("moduleID=@moduleID and version='E'");

        if (dt.Rows.Count > 0)
        {
            content.Text = dt.Rows[0]["content"].ToString();
            ViewState["ID"] = dt.Rows[0]["id"].ToString();


            DataRow row = ModulePublish.fillPageControlsByID(ViewState["ID"].ToString(), Page, "ContentPlaceHolder1");
            if (endDate.Text == "2800/1/1")
            {
                endDate.Text = "";
            }
            if (string.IsNullOrEmpty(row["fileUrl"].ToString()))
            {
                fileUrlLink.Visible = false;
            }
            else
            {
                fileUrlLink.NavigateUrl = "~/UploadFiles/Files/" + row["fileUrl"].ToString();
            }
            if (string.IsNullOrEmpty(row["pdfUrl"].ToString()))
            {
                pdfUrlLink.Visible = false;
            }
            else
            {
                pdfUrlLink.NavigateUrl = "~/UploadFiles/Files/" + row["pdfUrl"].ToString();
            }

            if (string.IsNullOrEmpty(row["picUrl"].ToString()))
            {
                fuPicLink.Visible = false;
            }
            else
            {
                fuPicLink.NavigateUrl = "~/UploadFiles/Images/" + row["picUrl"].ToString();
            }
        }
    }
    protected void InsertButton_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(content.Text))
        {
            lblError.Text = "請輸入資料";
            return;
        }
        EasyDataProvide ModulePublish = new EasyDataProvide("ModulePublish");

        if (ViewState["ID"] != null)
        {

            ModulePublish.setPageFormQuest(Page, "ContentPlaceHolder1");
            ModulePublish.UpdateById(ViewState["ID"].ToString());

            //取得寫入LOG
            EasyDataProvide Log = new EasyDataProvide("Log");
            string[] strUserData = ((FormsIdentity)(Page.User.Identity)).Ticket.UserData.Split(new Char[] { ';' });
            Log.addParameter("poster", strUserData[2]);
            Log.addParameter("subject", "編輯一筆資料");
            Log.addParameter("version", "E");
            Log.addParameter("moduleID", "d03");
            Log.Insert();


        }
        else
        {
            ModulePublish.addParameter("id", Guid.NewGuid().ToString());
            ModulePublish.setPageFormQuest(Page, "ContentPlaceHolder1");
            ModulePublish.addParameter("version", "E");
            ModulePublish.addParameter("moduleID", "d03");
            ModulePublish.Insert();
        }
        Response.Redirect("organization_Edit.aspx?ModuleID=" + "d03");
    }
    protected void ddlFile_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlFile.SelectedValue == "檔案上傳")
        {
            fuFile.Visible = true;
            fileUrl.Visible = false;
        }
        else
        {
            fuFile.Visible = false;
            fileUrl.Visible = true;
        }
    }
    protected void ddlPDF_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlPDF.SelectedValue == "檔案上傳")
        {
            fuPDF.Visible = true;
            pdfUrl.Visible = false;
        }
        else
        {
            fuPDF.Visible = false;
            pdfUrl.Visible = true;
        }
    }
}