﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class manage_B02FAQ_FAQ_list : System.Web.UI.Page
{
    #region "將目前功能資料送到Master Page裡"
    protected void Page_Init(object sender, EventArgs e)
    {

        HiddenField FunctionNumber = (HiddenField)Master.FindControl("FunctionNumber");
        FunctionNumber.Value = "j01";
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            showClass();
            show();
        }
    }
    private void showClass()
    {
        EasyDataProvide ModuleClass = new EasyDataProvide("ModuleClass");
        ModuleClass.addParameter("moduleID", "j01");
        DataTable dtProductClass = ModuleClass.getData(String.Format("version='E' and moduleID=?"), "order by listNum asc");
        ddlClass.DataTextField = "className";
        ddlClass.DataValueField = "id";
        ddlClass.DataSource = dtProductClass;
        ddlClass.DataBind();
        //ListItem item = new ListItem("全部", "");
        //ddlClass.Items.Insert(0, item);
    }
    protected void ddlClass_SelectedIndexChanged(object sender, EventArgs e)
    {
        show();
    }
    private void show()
    {
        EasyDataProvide ModulePublish = new EasyDataProvide("ModulePublish");
        ModulePublish.addParameter("moduleID", "j01");
        ModulePublish.addParameter("classID", ddlClass.SelectedValue);
        DataTable dataTable = ModulePublish.getData(String.Format("version='E' and moduleID=? and classID=?"), "order by listNum asc");
        GridView1.DataSource = dataTable;
        GridView1.DataBind();
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string strID = GridView1.DataKeys[e.RowIndex].Value.ToString();
        EasyDataProvide ModulePublish = new EasyDataProvide("ModulePublish");
        ModulePublish.DeleteById(strID);

        //取得寫入LOG
        EasyDataProvide Log = new EasyDataProvide("Log");
        string[] strUserData = ((FormsIdentity)(Page.User.Identity)).Ticket.UserData.Split(new Char[] { ';' });
        Log.addParameter("poster", strUserData[2]);
        Log.addParameter("subject", "刪除一筆資料");
        Log.addParameter("version", "E");
        Log.addParameter("moduleID", "j01");
        Log.Insert();
        show();
    }
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string strID = GridView1.DataKeys[e.RowIndex].Value.ToString();
        string page = Request["page"] ?? "1";
        Response.Redirect("FAQ_Edit.aspx?ID=" + strID + "&ModuleID=" + Request["ModuleID"] + "&page=" + page);
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        show();
    }
}