﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class manage_client_opinion_List : System.Web.UI.Page
{
    #region "將目前功能資料送到Master Page裡"
    protected void Page_Init(object sender, EventArgs e)
    {

        HiddenField FunctionNumber = (HiddenField)Master.FindControl("FunctionNumber");
        FunctionNumber.Value = "i03";
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            show();
        }
    }

    private void show()
    {
        EasyDataProvide Contact = new EasyDataProvide("Contact");
        Contact.addParameter("moduleID", "i03");
        DataTable dataTable = Contact.getData(String.Format("version='E' and moduleID=?"), "order by initDate desc");
        GridView1.DataSource = dataTable;
        GridView1.DataBind();
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string strID = GridView1.DataKeys[e.RowIndex].Value.ToString();
        EasyDataProvide Contact = new EasyDataProvide("Contact");
        Contact.DeleteById(strID);

        //取得寫入LOG
        EasyDataProvide Log = new EasyDataProvide("Log");
        string[] strUserData = ((FormsIdentity)(Page.User.Identity)).Ticket.UserData.Split(new Char[] { ';' });
        Log.addParameter("poster", strUserData[2]);
        Log.addParameter("subject", "刪除一筆資料");
        Log.addParameter("version", "E");
        Log.addParameter("moduleID", "i03");
        Log.Insert();
        show();
    }
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string strID = GridView1.DataKeys[e.RowIndex].Value.ToString();
        string page = Request["page"] ?? "1";
        //EasyDataProvide ModulePublish = new EasyDataProvide("ModulePublish");
        //DataRow row = ModulePublish.getByID(strID);
        //string classID = row["classID"].ToString();


        //    Response.Redirect("activity_MovEdit.aspx?ID=" + strID + "&ModuleID=" + Request["ModuleID"] + "&classID=" + classID + "&page=" + page);


    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        show();
    }
}