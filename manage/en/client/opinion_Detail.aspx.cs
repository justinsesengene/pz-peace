﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class manage_client_opinion_Detail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            show();
        }
    }
    private void show()
    {
        EasyDataProvide Contact = new EasyDataProvide("Contact");

        DataRow row = Contact.fillPageControlsByID(Request["ID"], Page, "ContentPlaceHolder1");
    }
}