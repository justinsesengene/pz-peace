﻿<%@ Page Title="" Language="C#" MasterPageFile="~/manage/MasterPage.master" AutoEventWireup="true" CodeFile="resources_Detail.aspx.cs" Inherits="manage_client_resources_Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/jquery-1.4.2.min.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/jquery.validate.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/jquery.metadata.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/ui.datepicker.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/ui.datepicker-zh-TW.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/jquery.maxlength-min.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/Scripts/ckeditor/ckeditor.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl(@"~/manage/javascript/thickbox.js") %>"></script>
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/jquery_validate.css") %>"
        rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/ui.core.css") %>"
        rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/ui.datepicker.css") %>"
        rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/ui.theme.css") %>"
        rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/single_seventeen.css") %>"
        rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/maxlength.css") %>"
        rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl(@"~/manage/css_styles/components/thickbox.css") %>" rel="stylesheet"
        type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () {
            $("form").validate({ meta: "validate" });
            $(".datepicker input").datepicker();
        });


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <fieldset>
<legend>聯絡人詳細內容</legend>
    <div style="display:inherit;text-align: left;"><asp:label ID="lblcompany" runat="server" AssociatedControlID="company">公司名稱</asp:label>
      <asp:label ID="company" runat="server"></asp:label>
    </div>
        <div style="display:inherit;text-align: left;"><asp:label ID="lblname" runat="server" AssociatedControlID="name">聯 絡 人</asp:label>
      <asp:label ID="name" runat="server"></asp:label>
    </div>
         <div style="display:inherit;text-align: left;"><asp:label ID="lblphoneNumber" runat="server" AssociatedControlID="phoneNumber">連絡電話</asp:label>
      <asp:label ID="phoneNumber" runat="server"></asp:label>
    </div>
        
        <div style="display:inherit;text-align: left;"><asp:label ID="lbladdress" runat="server" AssociatedControlID="address">地　　址</asp:label>
      <asp:label ID="address" runat="server"></asp:label>
    </div>
<div style="display:inherit;text-align: left;"><asp:label ID="lblemailAddress" runat="server" AssociatedControlID="emailAddress">E m a i l</asp:label>
      <asp:label ID="emailAddress" runat="server"></asp:label>
    </div>
        <div style="display:inherit;text-align: left;"><asp:label ID="lblarticle" runat="server" AssociatedControlID="article">內　　容</asp:label>
      <asp:label ID="article" runat="server"></asp:label>
    </div>
        <div style="display:inherit;text-align: left;"><asp:label ID="lblknowHow" runat="server" AssociatedControlID="knowHow">得知連全興</asp:label>
      <asp:label ID="knowHow" runat="server"></asp:label>
    </div>

       
</fieldset>
    <div style="display:inherit;text-align: left;">
<input type="button" value="回列表" onclick="history.back()"/></div>

</asp:Content>

