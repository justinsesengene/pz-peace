﻿<%@ Page Title="" Language="C#" MasterPageFile="~/manage/MasterPage.master" AutoEventWireup="true" CodeFile="Log_List.aspx.cs" Inherits="manage_Log_Log_List" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div>
       <div style="clear:both"></div>
      </div>

    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        CssClass="tableStyle1" DataKeyNames="id" 
        EnableModelValidation="True" GridLines="None" 
        onrowdeleting="GridView1_RowDeleting" 
        onrowupdating="GridView1_RowUpdating" AllowPaging="True" 
        onpageindexchanging="GridView1_PageIndexChanging">
        <Columns>
            <asp:TemplateField HeaderText="異動版本" SortExpression="version" Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="lblversion" runat="server" Text='<%# Eval("version").ToString() == "C" ?"中文版":"英文版" %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            <asp:BoundField DataField="poster" HeaderText="異動者" 
                SortExpression="poster" />
              <asp:TemplateField HeaderText="異動訊息" SortExpression="subjecT">
                    <ItemTemplate>
                        <span style="color: red">
                        <asp:Label ID="lblmoduleID" runat="server" Text='<%# EasyCode.ToEasyModuleName(Eval("moduleID").ToString()) %>'></asp:Label>
                            </span>
                            ，
                        <asp:Label ID="lblsubject" runat="server" Text='<%# Eval("subject") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            <asp:BoundField DataField="initDate" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HeaderText="異動時間" 
                SortExpression="initDate" />
                <asp:TemplateField HeaderText="編輯" Visible="False">
                    <ItemTemplate>
                        <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="編輯" 
                            CommandName="Update" ImageUrl="../images/Modify.gif" />
                    </ItemTemplate>
                   
                </asp:TemplateField>
                <asp:TemplateField HeaderText="刪除" ShowHeader="False" Visible="False">
                    <ItemTemplate>
                        <asp:ImageButton ID="ibtnDelete" runat="server" CssClass="action"  CommandName="Delete" ImageUrl="../images/Delete.gif" OnClientClick="javascript:if(!window.confirm('你確定要刪除嗎?')) window.event.returnValue = false;" />
                    </ItemTemplate>
                    
             </asp:TemplateField>
        </Columns>

    </asp:GridView>
</asp:Content>
