﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class manage_Ad_Ad_Insert : System.Web.UI.Page
{
    #region "將目前功能資料送到Master Page裡"
    protected void Page_Init(object sender, EventArgs e)
    {

        HiddenField FunctionNumber = (HiddenField)Master.FindControl("FunctionNumber");
        FunctionNumber.Value = "C01";
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void InsertButton_Click(object sender, EventArgs e)
    {
        //if (string.IsNullOrEmpty(content.Text))
        //{
        //    lblError.Text = "請輸入資料";
        //    return;
        //}
        EasyDataProvide ModulePublish = new EasyDataProvide("ModulePublish");
        ModulePublish.addParameter("id", Guid.NewGuid().ToString());
        ModulePublish.setPageFormQuest(Page, "ContentPlaceHolder1");
        //處理上傳檔案
        if (ddlFile.SelectedValue == "檔案上傳" && fuFile.HasFile)
        {
            //取得副檔名
            string Extension = fuFile.FileName.Split('.')[fuFile.FileName.Split('.').Length - 1];
            //新檔案名稱
            string fileName = String.Format("{0:yyyyMMddhhmmsss}.{1}", DateTime.Now, Extension);
            fuFile.SaveAs(Server.MapPath(String.Format("~/UploadFiles/Files/{0}", fileName)));
            ModulePublish.addParameter("fileUrl", fileName);

        }
        System.Threading.Thread.Sleep(1000);
        //處理上傳PDF檔案
        if (ddlPDF.SelectedValue == "檔案上傳" && fuPDF.HasFile)
        {
            //取得副檔名
            string Extension = fuPDF.FileName.Split('.')[fuPDF.FileName.Split('.').Length - 1];
            if (Extension.ToLower() != "pdf")
            {
                My.WebForm.doJavaScript("alert('pdf檔錯誤!');", Page);
                return;
            }
            //新檔案名稱
            string fileName = String.Format("{0:yyyyMMddhhmmsss}.{1}", DateTime.Now, Extension);
            fuPDF.SaveAs(Server.MapPath(String.Format("~/UploadFiles/Files/{0}", fileName)));
            ModulePublish.addParameter("pdfUrl", fileName);

        }
        //處理上傳圖片

        if (fuPic.HasFile)
        {
            if (fuPic.PostedFile.ContentType.IndexOf("image") == -1)
            {
                My.WebForm.doJavaScript("alert('檔案型態錯誤!');", Page);
                return;
            }

            //取得副檔名
            string Extension = fuPic.FileName.Split('.')[fuPic.FileName.Split('.').Length - 1];
            //新檔案名稱
            string fileName = String.Format("{0:yyyyMMddhhmmsss}.{1}", DateTime.Now, Extension);
            fuPic.SaveAs(Server.MapPath(String.Format("~/UploadFiles/Images/{0}", fileName)));
            ModulePublish.addParameter("picUrl", fileName);

        }

        ModulePublish.addParameter("version", "C");

        ModulePublish.addParameter("moduleID", "C01");

        //不填結束日期時，設定一個800年後的日期
        if (string.IsNullOrEmpty(endDate.Text))
        {
            ModulePublish.addParameter("endDate", "2800/1/1");
        }
        ModulePublish.Insert();
        //取得寫入LOG
        EasyDataProvide Log = new EasyDataProvide("Log");
        string[] strUserData = ((FormsIdentity)(Page.User.Identity)).Ticket.UserData.Split(new Char[] { ';' });
        Log.addParameter("poster", strUserData[2]);
        Log.addParameter("subject", "新增一筆資料");
        Log.addParameter("version", "C");
        Log.addParameter("moduleID", "C01");
        Log.Insert();

        Response.Redirect("Ad_List.aspx?ModuleID=" + "C01");
    }
    protected void ddlFile_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlFile.SelectedValue == "檔案上傳")
        {
            fuFile.Visible = true;
            fileUrl.Visible = false;
        }
        else
        {
            fuFile.Visible = false;
            fileUrl.Visible = true;
        }


    }
    protected void ddlPDF_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlPDF.SelectedValue == "檔案上傳")
        {
            fuPDF.Visible = true;
            pdfUrl.Visible = false;
        }
        else
        {
            fuPDF.Visible = false;
            pdfUrl.Visible = true;
        }
    }
}